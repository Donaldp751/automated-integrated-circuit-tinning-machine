﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NVE_IC_Tinning_Control_Panel
{
    class LotInfo
    {
        private string LotNumber;
        private string Operator;
        private DateTime DateAndTimeOfStart;
        private int PartsCompletedInLot = 0;
        private int ErrorsDuringOperation = 0;
        public LotInfo()
        {
            
        }
        
        public LotInfo(string LotNumber)
        {
            bool lotDirectoryExists = false;
            bool currentLotDirectoryExists = false;
            foreach (var directory in Directory.GetDirectories(Directory.GetCurrentDirectory()))
            {
                if (directory.Equals("LotData"))
                {
                    lotDirectoryExists = true;
                }
            }

            if (!lotDirectoryExists)
            {
                Directory.CreateDirectory("LotData");
            }

            foreach (var directory in Directory.GetDirectories("LotData"))
            {
                if (directory.Equals(LotNumber))
                {
                    currentLotDirectoryExists = true;
                }
            }
            if (!currentLotDirectoryExists)
            {
                Directory.CreateDirectory("LotData\\" + LotNumber);
            }
        }
        private bool DoesLotDirExists()
        {
            bool lotDirectoryExists = false;
            foreach (var directory in Directory.GetDirectories(Directory.GetCurrentDirectory()))
            {
                if (directory.Equals("LotData"))
                {
                    lotDirectoryExists = true;
                }
            }
            return lotDirectoryExists;
        }
        private bool DoesCurrentLotDirectoryExist()
        {
            bool currentLotDirectoryExists = false;
            foreach (var directory in Directory.GetDirectories("LotData"))
            {
                if (directory.Equals(LotNumber))
                {
                    currentLotDirectoryExists = true;
                }
            }
            return currentLotDirectoryExists;
        }
        public int GetTotalPartsCompletedInLot()
        {
            int totalPartsCompleted = 0;
            if (DoesLotDirExists() && DoesCurrentLotDirectoryExist())
            {
                foreach (var file in Directory.GetFiles("LotData\\" + LotNumber))
                {
                    string lotInfoFileContent = File.ReadAllText(file);
                    LotInfo li = JsonConvert.DeserializeObject<LotInfo>(lotInfoFileContent);
                    totalPartsCompleted += li.GetPartsCompleted();
                }
            }
            return 0;
        }
        public int GetPartsCompleted()
        {
            return PartsCompletedInLot;
        }
        public void PartCompleted()
        {
            PartsCompletedInLot++;
        }
        public void IncrementLotError()
        {
            ErrorsDuringOperation++;
        }
        public bool SetOperatorName(string opName)
        {
            if(opName?.Length < 1)
            {
                return false;
            }
            else
            {
                Operator = opName;
                return true;
            }
        }
        public bool SetLotNumber(string lotNumber)
        {
            if(lotNumber?.Length < 1)
            {
                return false;
            }else
            {
                LotNumber = lotNumber;
                return true;
            }
        }
        public void UpdateStartTime()
        {
            DateAndTimeOfStart = DateTime.Now;
        }
        public bool SaveRunToDisk()
        {
            bool lotDirectoryExists = false;
            bool currentLotDirectoryExists = false;
            foreach(var directory in Directory.GetDirectories(""))
            {
                if (directory.Equals("LotData"))
                {
                    lotDirectoryExists = true;
                }
            }

            if (!lotDirectoryExists)
            {
                Directory.CreateDirectory("LotData");
            }

            foreach (var directory in Directory.GetDirectories("LotData"))
            {
                if (directory.Equals(this.LotNumber))
                {
                    currentLotDirectoryExists = true;
                }
            }

            if (!currentLotDirectoryExists)
            {
                Directory.CreateDirectory("LotData\\" + this.LotNumber);
            }

            string serializedLotInfo = JsonConvert.SerializeObject(this);
            string fileName = DateTime.Now.ToFileTime().ToString();

            try
            {
                File.WriteAllText(fileName, serializedLotInfo);
                return true;
            }
            catch(Exception e)
            {
                Debug.WriteLine("Failed to write lot info file to disk");
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}
