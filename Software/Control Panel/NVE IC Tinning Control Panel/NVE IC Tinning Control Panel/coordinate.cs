﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NVE_IC_Tinning_Control_Panel
{
    class coordinate
    {
        public double x; //x position in mm
        public double y; //y position in mm
        public double z; //z position in mm
        public double yaw; //rotation around the z axis in degrees (otherwise known as A axis)
        public coordinate(double x, double y, double z, double yaw)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.yaw = yaw;
        }
        public override string ToString()
        {
            return x.ToString() + "," + y.ToString()+ "," + z.ToString() + "," + yaw.ToString();
        }


    }

}
