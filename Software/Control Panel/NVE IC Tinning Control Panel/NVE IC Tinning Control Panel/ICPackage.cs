﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;

namespace NVE_IC_Tinning_Control_Panel
{
    class ICPackage
    {
        public ICPackage()
        {

        }
        public bool SaveToFile()
        {
            try
            {
                SaveToFile(GetPackagePath(this.Name));
                return true;
            }
            catch
            {
                return false;
            }

        }
        static public bool DeletePackageFromDisk(string name)
        {
            try
            {
                string path = GetPackagePath(name);
                bool exists = File.Exists(path);
                if (exists)
                {
                    File.Delete(path);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool SaveToFile(String path)
        {
            var textFile = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(path, textFile);

            return true;
        }
        static private string GetPackagePath(string name)
        {
            return "packages\\" + name + ".NVE-PKG";
        }
        static public ICPackage LoadFromFile(string name)
        {
            string path = GetPackagePath(name);
            try
            {
                var textFile = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<ICPackage>(textFile);
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }
        public String Name { get; set; } //name of the package type, ex WB SOIC 8, or NB SOIC 8
        public double Width { get; set; } //Body Width, one side of pins to another
        public double Length { get; set; } //Body length, front to back of IC
        public double LegLength { get; set; } //how far do the legs protrude off the body
        public bool IsSurfaceMount { get; set; } //Indicates whether surface mount or through-hole 
        public int PreheatSoakTime { get; set; } //number of milli-seconds to preheat part
        public float ClearZHeight { get; set; } //height
        public int TravelSpeed { get; set; } //how fast to move inbetween actions
        public int ActionSpeed { get; set; } //how fast to move during operations/actions (like tinning that part, fluxing, etc)
        public coordinate PickupLocation { get; set; } //location to pickup IC at start of process
        public coordinate PreheatLocation { get; set; } //location to preheat IC 
        public coordinate FluxFountainEntryLocation { get; set; } //location to enter and exit the flux fountain at
        public coordinate FluxFountainWettingLocation1 { get; set; } //position to wet IC first
        public coordinate FluxFountainWettingLocation2 { get; set; } //position to wet IC second
        public coordinate SolderPotEntryLocation { get; set; } //position to enter the solder pot at
        public coordinate SolderPotWettingLocation1 { get; set; } //position to wet the first side at
        public coordinate SolderPotWettingLocation2 { get; set; } //position to wet the second side at
        public coordinate DropoffLocation { get; set; } //position to drop the IC off after tinning has completed
    }
}
