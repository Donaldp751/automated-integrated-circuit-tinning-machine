﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace NVE_IC_Tinning_Control_Panel
{
    class InputOutputFeedbackUpdateModel : INotifyPropertyChanged
    {
        private UInt16 _inputWord = 0;
        private UInt16 _outputWord = 0;
        SolidColorBrush ActiveColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));
        SolidColorBrush InActiveColor = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        public UInt16 InputWord
        {
            set
            {
                _inputWord = value;
                OnPropertyChanged();
            }
        }
        public UInt16 OutputWord
        {
            set
            {
                _outputWord = value;
                OnPropertyChanged();
            }
        }
        public SolidColorBrush SFZeroColor
        {
            get
            {
                if (((int)_inputWord & 1 ) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFOneColor
        {
            get
            {
                if (((int)_inputWord & 1<<1) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }

        public SolidColorBrush SFTwoColor
        {
            get
            {
                if (((int)_inputWord & 1<<2) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFThreeColor
        {
            get
            {
                if (((int)_inputWord & 1<<3) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFFourColor
        {
            get
            {
                if (((int)_inputWord & 1<<4) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFFiveColor
        {
            get
            {
                if (((int)_inputWord & 1<<5) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFSixColor
        {
            get
            {
                if (((int)_inputWord & 1 << 6) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFSevenColor
        {
            get
            {
                if (((int)_inputWord & 1 << 7) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush SFEightColor
        {
            get
            {
                if (((int)_inputWord & 1 << 8) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFZeroColor
        {
            get
            {
                if (((int)_outputWord & 1 << 0) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFOneColor
        {
            get
            {
                if (((int)_outputWord & 1 << 1) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFTwoColor
        {
            get
            {
                if (((int)_outputWord & 1 << 2) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFThreeColor
        {
            get
            {
                if (((int)_outputWord & 1 << 3) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFFourColor
        {
            get
            {
                if (((int)_outputWord & 1 << 4) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFFiveColor
        {
            get
            {
                if (((int)_outputWord & 1 << 5) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFSixColor
        {
            get
            {
                if (((int)_outputWord & 1 << 6) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFSevenColor
        {
            get
            {
                if (((int)_outputWord & 1 << 7) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFEightColor
        {
            get
            {
                if (((int)_outputWord & 1 << 8) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFNineColor
        {
            get
            {
                if (((int)_outputWord & 1 << 9) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        public SolidColorBrush AFTenColor
        {
            get
            {
                if (((int)_outputWord & 1 << 10) != 0)
                {
                    return ActiveColor;
                }
                else
                {
                    return InActiveColor;
                }
            }
        }
        private string _currentState = "";
        public string CurrentState
        {
            get
            {
                return _currentState;
            }
            set
            {
                _currentState = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            //PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            if (propertyName.Equals("InputWord"))
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFZeroColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFOneColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFTwoColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFThreeColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFFourColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFFiveColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFSixColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFSevenColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SFEightColor"));

            }
            else if (propertyName.Equals("OutputWord"))
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFZeroColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFOneColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFTwoColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFThreeColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFFourColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFFiveColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFSixColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFSevenColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFEightColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFNineColor"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AFTenColor"));
            }else if (propertyName.Equals("CurrentState"))
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentState"));
            }
            
        }
    }
}
