﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NVE_IC_Tinning_Control_Panel
{
    /// <summary>
    /// Interaction logic for DiagnosticLogin.xaml
    /// </summary>
    public partial class DiagnosticLogin : Window
    {
        public bool LoginSuccessful = false;
        private string password = "4444";
        public DiagnosticLogin()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginSuccessful = PasswordEntryBox.Password == password;

            if (PasswordEntryBox.Password != password)
            {
              Button clickedButton = sender as Button;
                clickedButton.Background = Brushes.Red;
            }
            else
            {
                Button clickedButton = sender as Button;
                clickedButton.Background = Brushes.Green;
                this.Close();
            }
        }
    }
}
