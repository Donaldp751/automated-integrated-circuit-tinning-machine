﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NVE_IC_Tinning_Control_Panel
{
    /// <summary>
    /// Interaction logic for PartEditor.xaml
    /// </summary>
    public partial class PartEditor : Window
    {
        public PartEditor(string packageName)
        {
            InitializeComponent();
            try
            {
                    if(packageName != null)
                {
                    PartNameTB.Text = packageName;
                    LoadButton_Click(this, new RoutedEventArgs());
                }
                else
                {
                    PartNameTB.Text = "Generic"; //dont load, just have default values to setup a new package
                }
                    
            }
            catch
            {

            }
        }

        public override void EndInit()
        {
            base.EndInit();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var messageBoxResult = MessageBox.Show("Are you sure you want to delete this package type??", "Remove " + PartNameTB.Text + " from disk",MessageBoxButton.YesNo);
            if(messageBoxResult == MessageBoxResult.Yes)
            {
                ICPackage.DeletePackageFromDisk(PartNameTB.Text);
            }
        }
        private coordinate parseCoordFromTB(TextBox tb)
        {
            String[] splitParts = tb.Text.Split(',');
            coordinate coord = new coordinate(Convert.ToDouble(splitParts[0]), Convert.ToDouble(splitParts[1]), Convert.ToDouble(splitParts[2]), Convert.ToDouble(splitParts[3]));
            return coord;
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            ICPackage ic = new ICPackage();
            try
            {
                ic.Name = this.PartNameTB.Text;
                ic.Width = Convert.ToDouble(this.PartWidthTB.Text);
                ic.Length = Convert.ToDouble(this.PartLengthTB.Text);
                ic.LegLength = Convert.ToDouble(this.LegLengthTB.Text);
                ic.IsSurfaceMount = IsSurfaceMountComponent.IsChecked == true;
                ic.PreheatSoakTime = Convert.ToInt32(PreheatSoakTimeTB.Text);
                ic.ActionSpeed = Convert.ToInt32(ActionSpeedTB.Text);
                ic.TravelSpeed = Convert.ToInt32(TravelSpeedTB.Text);
                ic.ClearZHeight = (float)Convert.ToDouble(ClearZHeightTB.Text);
                ic.PickupLocation = parseCoordFromTB(PickupLocationTB);
                ic.PreheatLocation = parseCoordFromTB(PreheatLocationTB);
                ic.FluxFountainEntryLocation = parseCoordFromTB(FluxEntryLocationTB);
                ic.FluxFountainWettingLocation1 = parseCoordFromTB(FluxWettingLocation1TB);
                ic.FluxFountainWettingLocation2 = parseCoordFromTB(FluxWettingLocation2TB);
                ic.SolderPotEntryLocation = parseCoordFromTB(SolderPotEntryTB);
                ic.SolderPotWettingLocation1 = parseCoordFromTB(SolderpotWetting1TB);
                ic.SolderPotWettingLocation2 = parseCoordFromTB(SolderpotWetting2TB);
                ic.DropoffLocation = parseCoordFromTB(DropOffPositionTB);


                ic.SaveToFile();
            }
            catch
            {
                ErrorWindow ew = new ErrorWindow();
                ew.ErrorTB.Text = "Failed to save to disk";
                ew.ShowDialog();
            }

        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            ICPackage ic = ICPackage.LoadFromFile(PartNameTB.Text);
            try
            {
                PartNameTB.Text = ic.Name;
                PartWidthTB.Text = ic.Width.ToString();
                PartLengthTB.Text = ic.Length.ToString();
                LegLengthTB.Text = ic.LegLength.ToString();
                ActionSpeedTB.Text = ic.ActionSpeed.ToString();
                TravelSpeedTB.Text = ic.TravelSpeed.ToString();
                IsSurfaceMountComponent.IsChecked = ic.IsSurfaceMount;
                PreheatSoakTimeTB.Text = ic.PreheatSoakTime.ToString();
                ClearZHeightTB.Text = ic.ClearZHeight.ToString();
                PickupLocationTB.Text = ic.PickupLocation.ToString();
                PreheatLocationTB.Text = ic.PreheatLocation.ToString();
                FluxEntryLocationTB.Text = ic.FluxFountainEntryLocation.ToString();
                FluxWettingLocation1TB.Text = ic.FluxFountainWettingLocation1.ToString();
                FluxWettingLocation2TB.Text = ic.FluxFountainWettingLocation2.ToString();
                SolderPotEntryTB.Text = ic.SolderPotEntryLocation.ToString();
                SolderpotWetting1TB.Text = ic.SolderPotWettingLocation1.ToString();
                SolderpotWetting2TB.Text = ic.SolderPotWettingLocation2.ToString();
                DropOffPositionTB.Text = ic.DropoffLocation.ToString();
            }
            catch
            {
                ErrorWindow ew = new ErrorWindow();
                ew.ErrorTB.Text = "Failed to load IC Package : " + PartNameTB.Text + ".NVE-PKG" + " From disk, please verify spelling of Package name and that it exists on disk";
                ew.ShowDialog();
            }
        }
    }
}
