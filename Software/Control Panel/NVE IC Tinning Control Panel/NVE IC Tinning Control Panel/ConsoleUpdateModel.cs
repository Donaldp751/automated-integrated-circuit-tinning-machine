﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NVE_IC_Tinning_Control_Panel
{
    class ConsoleUpdateModel : INotifyPropertyChanged
    {
            private List<string> _consoleWindowContents;
            
            private string connectedString = "Connected";
            private string notConnectedString = "Not Connected";
            private string _connectionStatusText;
            public string ConsoleContentsText
            {
                get {
                string consoleContents = "";
                int i = 0;
                if(_consoleWindowContents.Count > 25)
                {
                    _consoleWindowContents.RemoveRange(1, 5);
                }
                foreach (string line in _consoleWindowContents)
                {
                    consoleContents = consoleContents + line + "\n" ;
                }
                
                return consoleContents; }
                set
                {
                _consoleWindowContents.Add(value);
                    OnPropertyChanged();
                }
            }

        public ConsoleUpdateModel()
        {
            _consoleWindowContents = new List<string>();
        }

            public void WriteLineToConsole(string message)
        {
            ConsoleContentsText = message;
        }


            public event PropertyChangedEventHandler PropertyChanged;
            protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        
    }
}
