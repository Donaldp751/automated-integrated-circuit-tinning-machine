﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NVE_IC_Tinning_Control_Panel
{
    public class AICTState
    {
        private StatusUpdateModel _statusUpdater;
        public enum MachineState
        {
            Disconnected,
            Standby,
            ReadyToRun,
            Upload,
            Homing,
            Running,
            Emtpy,
            OutputFull,
            Stopping,
            Error
        };

        public AICTState(StatusUpdateModel statusUpdateModel)
        {
            _statusUpdater = statusUpdateModel;
        }

        private MachineState _machineState;

        public void SetMachineState(MachineState ms)
        {
            _machineState = ms;
            _statusUpdater.SetUIStatus(this.GetMachineStateStr());
        }
        public MachineState GetMachineState()
        {
            return _machineState;
        }
        public String GetMachineStateStr()
        {
            switch (_machineState)
            {
                case MachineState.Disconnected:
                    return "Disconnected";

                case MachineState.Standby:
                    return "Standby";

                case MachineState.ReadyToRun:
                    return "Ready to Run";

                case MachineState.Upload:
                    return "Uploading parameters";

                case MachineState.Homing:
                    return "Homing Machine";

                case MachineState.Running:
                    return "Running";

                case MachineState.Stopping:
                    return "Stopping";

                case MachineState.Emtpy:
                    return "Needs reload";

                case MachineState.OutputFull:
                    return "Output full";

                case MachineState.Error:
                    return "ERROR";
            }

            return "";
        }
    }
}
