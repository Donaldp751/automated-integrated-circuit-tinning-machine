﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace NVE_IC_Tinning_Control_Panel
{
    public class StatusUpdateModel : INotifyPropertyChanged
    {
        private string connectedString = "Connected";
        private string notConnectedString = "Not Connected";
        private string _connectionStatusText;
        public string ConnectionStatusText
        {
            get { return _connectionStatusText; }
            set
            {
                _connectionStatusText = value;
                OnPropertyChanged();
            }
        }
        public void SetUIStatus(string statusString)
        {
            ConnectionStatusText = statusString;
        }
        public void SetUIConnected()
        {
            ConnectionStatusText = this.connectedString;
        }
        public void SetUIDisconnected()
        {
            ConnectionStatusText = this.notConnectedString;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
