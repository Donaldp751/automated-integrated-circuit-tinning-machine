﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace NVE_IC_Tinning_Control_Panel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int maxTimeout = 5; //seconds without a heartbeat before timing out
        int timeoutCounter = 0;

        static Timer TTimer; //1s tick timer
        
        
        string serialPort = "COM7";
        bool ParametersSuccessfullyUploaded = false;
        bool authorizedUser = false;
        bool embeddedFeedbackEnabled = true;
        bool CurrentlyRunning = false;
        bool connectionConfirmed = false;
        StatusUpdateModel connectionStatus = new StatusUpdateModel();
        StatusUpdateModel machineStatus = new StatusUpdateModel();
        AICTState AICTStateController;
        ConsoleUpdateModel consoleUpdateModel = new ConsoleUpdateModel();
        InputOutputFeedbackUpdateModel feedbackUpdateModel = new InputOutputFeedbackUpdateModel();
        EmbeddedCommunication embedded;
        LotInfo currentLotInfo;
        private string[] LoadPackageNamesFromDisk()
        {
            List<string> packageNames = new List<string>();
            string currentDirectory = Directory.GetCurrentDirectory() + "\\packages";
            DirectoryInfo di = new DirectoryInfo(currentDirectory);

            if (!di.Exists)
            {
                di.Create();
            }

            foreach (var file in di.GetFiles())
            {
                if (file.Extension == ".NVE-PKG")
                {
                    packageNames.Add(file.Name.Split('.')[0]);
                }
            }

            return packageNames.ToArray();
        }
        public MainWindow()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.AutoFlush = true;


            InitializeComponent();

            StartButton.IsEnabled = false;
            PauseButton.IsEnabled = false;
            StopButton.IsEnabled = false;
            UploadParametersBtn.IsEnabled = false;
            HomeMachineBtn.IsEnabled = true;

            /*
            List<String> packageNames = new List<String>();
            
            packageNames.Add("NB SOIC 8");
            packageNames.Add("NB SOIC 16");
            packageNames.Add("WB SOIC 16");
            packageNames.Add("PDIP 16");

            var comboBox = PackageTypesCB;
            comboBox.ItemsSource = packageNames;
            comboBox.SelectedIndex = 0;
            */

            PackageTypesCB.ItemsSource = LoadPackageNamesFromDisk();
            PackageTypesCB.SelectedIndex = 0;

            connectionStatus.SetUIDisconnected();
            ConnectionStatusTB.DataContext = connectionStatus;
            MachineStatusTB.DataContext = machineStatus;

            ConsoleTB.DataContext = consoleUpdateModel;
            //consoleUpdateModel.WriteLineToConsole("Testing");

            SFRectZero.DataContext = feedbackUpdateModel;
            SFRectOne.DataContext = feedbackUpdateModel;
            SFRectTwo.DataContext = feedbackUpdateModel;
            SFRectThree.DataContext = feedbackUpdateModel;
            SFRectFour.DataContext = feedbackUpdateModel;
            SFRectFive.DataContext = feedbackUpdateModel;
            SFRectSix.DataContext = feedbackUpdateModel;
            SFRectSeven.DataContext = feedbackUpdateModel;
            SFRectEight.DataContext = feedbackUpdateModel;

            AFRectZero.DataContext = feedbackUpdateModel;
            AFRectOne.DataContext = feedbackUpdateModel;
            AFRectTwo.DataContext = feedbackUpdateModel;
            AFRectThree.DataContext = feedbackUpdateModel;
            AFRectFour.DataContext = feedbackUpdateModel;
            AFRectFive.DataContext = feedbackUpdateModel;
            AFRectSix.DataContext = feedbackUpdateModel;
            AFRectSeven.DataContext = feedbackUpdateModel;
            AFRectEight.DataContext = feedbackUpdateModel;
            AFRectNine.DataContext = feedbackUpdateModel;
            AFRectTen.DataContext = feedbackUpdateModel;

            CurrentStateTB.DataContext = feedbackUpdateModel;

            embedded = new EmbeddedCommunication();
            embedded.ConnectToEmbeddedSystemPort();

            TTimer = new Timer(new TimerCallback(oneSecondTickTimer), null, 3000, 500); //Wait 3 seconds to allow app to settle, then check connection every half second
            AICTStateController = new AICTState(machineStatus);
            AICTStateController.SetMachineState(AICTState.MachineState.Disconnected);
            connectionStatus.SetUIDisconnected();
        }

        enum _IC_Plating_Process
        {
            waitForStart, //wait till the operator starts
            moveAbovePickupLocation, //Sends motion commands, wait not required
            waitForPart,
            moveToPreheatLocation, //move the pickhead above the preheat location
            preheatIC, //lower part to preheat location
            moveToFluxEntryLocation, //move to the entry location
            applyFluxToLeads, //move to the first flux location, then back, rotate, and flux the otherside of the part
            moveToSolderPotEntryLocation, //move the IC above the solder pot to the entry position for tinning
            applySolderToLeads, //tinning process
            moveToDropoffLocation, //move to the output track
            releasePart, //drop the IC
            waitForOutputPart, //wait till the part passes the output sensor
            stopIfFull, //stop Processing parts if the output is full
            stop //stop immediately, turn off equipment
        };
        private string updateMachineStatusFromStatusWord(UInt16 statusWord)
        {
            UInt16 sw = (ushort)((statusWord) >> 12);

            //Debug.WriteLine("status word = " + statusWord.ToString() + "    " + sw.ToString());
            
            _IC_Plating_Process status = (_IC_Plating_Process)sw;

            //string machineStatus;

            switch (status)
            {
                case _IC_Plating_Process.waitForStart: //wait till the operator starts
                    machineStatus.ConnectionStatusText = "Waiting to start";
                    break;
                case _IC_Plating_Process.moveAbovePickupLocation:
                    machineStatus.ConnectionStatusText = "Moving to IT";
                    break;
                case _IC_Plating_Process.waitForPart:
                    machineStatus.ConnectionStatusText = "Waiting for part";
                    break;
                case _IC_Plating_Process.moveToPreheatLocation: //move the pickhead above the preheat location
                    machineStatus.ConnectionStatusText = "Moving to PH";
                    break;
                case _IC_Plating_Process.preheatIC:  //lower part to preheat location
                    machineStatus.ConnectionStatusText = "Heating part";
                    break;
                case _IC_Plating_Process.moveToFluxEntryLocation: //move to the flux entry location
                    machineStatus.ConnectionStatusText = "Moving to FF";
                    break;
                case _IC_Plating_Process.applyFluxToLeads: //move to the first flux location, then back, rotate, and flux the otherside of the part
                    machineStatus.ConnectionStatusText = "Flux application";
                    break;
                case _IC_Plating_Process.moveToSolderPotEntryLocation: //move the IC above the solder pot to the entry position for tinning
                    machineStatus.ConnectionStatusText = "Moving to SP";
                    break;
                case _IC_Plating_Process.applySolderToLeads: //move the IC above the solder pot to the entry position for tinning
                    machineStatus.ConnectionStatusText = "Tinning IC";
                    break;
                case _IC_Plating_Process.moveToDropoffLocation: //move to the output track
                    machineStatus.ConnectionStatusText = "Moving to OT";
                    break;
                case _IC_Plating_Process.releasePart:  //drop the IC
                    machineStatus.ConnectionStatusText = "Depositing IC";
                    break;
                case _IC_Plating_Process.waitForOutputPart:
                    machineStatus.ConnectionStatusText = "Waiting on OT";
                    break;
                case _IC_Plating_Process.stopIfFull:  //drop the IC
                    machineStatus.ConnectionStatusText = "OT Full";
                    break;
                case _IC_Plating_Process.stop: //turn off everything and go back to standby
                    machineStatus.ConnectionStatusText = "Stopping..";
                    break;
            }

            return null;
        }
        private void oneSecondTickTimer(Object state) //Gets called once per half second
        {
            bool newHeartbeat = embedded.IsHeartbeatReceived;
            connectionConfirmed |= newHeartbeat; //has responded at least once

            embedded.TransmitHeartbeat(embeddedFeedbackEnabled); //send a heartbeat request to the embedded
            //return;

            if (newHeartbeat)
            {
                timeoutCounter = 0;
            }
            else
            {
                timeoutCounter++;
            }

            if ((timeoutCounter < maxTimeout) && connectionConfirmed) //embeddedFeedbackEnabled
            {
                feedbackUpdateModel.InputWord = embedded.GetInputWord();
                feedbackUpdateModel.OutputWord = embedded.GetOutputWord();
                updateMachineStatusFromStatusWord(embedded.GetStatusWord());
                feedbackUpdateModel.CurrentState = "Connected, ready to run";
                connectionStatus.SetUIConnected();
                if (AICTStateController.GetMachineState() == AICTState.MachineState.Disconnected)
                {
                    AICTStateController.SetMachineState(AICTState.MachineState.Standby);
                }
                //consoleUpdateModel.WriteLineToConsole("Connection gucci");
            }
            else
            {
                feedbackUpdateModel.InputWord = UInt16.MinValue;
                feedbackUpdateModel.OutputWord = UInt16.MinValue;
                feedbackUpdateModel.CurrentState = "Disconnected, not ready";
                connectionStatus.SetUIDisconnected();
                switch (AICTStateController.GetMachineState())
                {
                    case AICTState.MachineState.Standby:
                            AICTStateController.SetMachineState(AICTState.MachineState.Disconnected);
                        break;

                    case AICTState.MachineState.Running:
                            AICTStateController.SetMachineState(AICTState.MachineState.Error);
                        break;
                }
            }
            string msgContents = embedded.getLatestMessageContents();
            if (msgContents != null)
            {
                consoleUpdateModel.WriteLineToConsole(msgContents);
            }
            
        }
        private void unauthorizedUserUIElements()
        {
            authorizedUser = false;
            AdminLogoutBtn.Visibility = Visibility.Hidden;
            AdminModeEnabledText.Visibility = Visibility.Hidden;
            ModifyPackageBtn.Visibility = Visibility.Hidden;
            ModifyOperatorNameBtn.Visibility = Visibility.Hidden;
        }
        private void authorizedUserUIElements()
        {
            authorizedUser = true;
            AdminLogoutBtn.Visibility = Visibility.Visible;
            AdminModeEnabledText.Visibility = Visibility.Visible;
            ModifyPackageBtn.Visibility = Visibility.Visible;
            ModifyOperatorNameBtn.Visibility = Visibility.Visible;
        }
        private string readConfigurationFromFile()
        {

            return "";
        }

        private void MouseClickedAbout(object sender, MouseButtonEventArgs e)
        {
            embedded.ConnectToEmbeddedSystemPort();

        }

        private void PackageTypesCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (embedded.IsConnected)
            {
                StartButton.IsEnabled = false;
                PauseButton.IsEnabled = true;
                StopButton.IsEnabled = true;
                embedded.StartAutomation();
            }
            else
            {
                
            }

           
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            StartButton.IsEnabled = true;
            PauseButton.IsEnabled = false;
            StopButton.IsEnabled = true;
            embedded.PauseAutomation();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StartButton.IsEnabled = true;
            PauseButton.IsEnabled = false;
            StopButton.IsEnabled = false;
            embedded.StopAutomation();
        }

        private void DiagnosticLogin_Click(object sender, RoutedEventArgs e)
        {
            var diagnosticLoginWindow = new DiagnosticLogin();
            diagnosticLoginWindow.ShowDialog();
            if (diagnosticLoginWindow.LoginSuccessful)
            {
                authorizedUserUIElements();
            }
            else
            {
                unauthorizedUserUIElements();
            }
        }

        private void AdminLogoutBtn_Clicked(object sender, RoutedEventArgs e)
        {
            unauthorizedUserUIElements();
        }
        private void ModifyPackageBtn_Clicked(object sender, RoutedEventArgs e)
        {
            string temp = PackageTypesCB.SelectedItem?.ToString();
            var partEditorWindow = new PartEditor(temp);

            partEditorWindow.ShowDialog();
            PackageTypesCB.ItemsSource = LoadPackageNamesFromDisk();
            foreach(string package in PackageTypesCB.ItemsSource)
            {
                if (package.Equals(partEditorWindow.PartNameTB.Text))
                {
                    PackageTypesCB.SelectedItem = package;
                    break;
                }
            }
        }

        private void ConnectionStatusTB_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void ConfirmLotInfoBtn_Clicked(object sender, RoutedEventArgs e)
        {
            currentLotInfo = new LotInfo(LotIDTB.Text.Trim());
            currentLotInfo.SetOperatorName(OperatorNameCB.Text);
            PartsTinnedQTYTB.Text = currentLotInfo.GetTotalPartsCompletedInLot().ToString();
            if (embedded.IsConnected)
            {
                UploadParametersBtn.IsEnabled = true;
            }
            else
            {

            }
            

        }

        private byte[] PrepareMessage(byte messageTypeCode, string messageContents)
        {
            int len = messageContents.Length;
            byte[] messageBuffer = Encoding.ASCII.GetBytes(messageContents);
            byte[] returnBuffer = new byte[len + 1];
            returnBuffer[0] = messageTypeCode;
            for (int i = 1; i < (len + 1); i++)
            {
                returnBuffer[i] = messageBuffer[i - 1];
            }
            return returnBuffer;
        }

        private bool MessageEmbedded(byte messageTypeCode, string messageContents)
        {
            try
            {
                if (!embedded.IsConnected)
                {
                    Debug.WriteLine("Cannot write message to embedded system while not connected..");
                    return false;
                }
                return embedded.WriteToEmbeddedSystem(PrepareMessage(messageTypeCode, messageContents));
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception thrown while writing to embedded system.");
                Debug.WriteLine(e);
            }

            return true;
        }



        private byte[] floatToFixedBytes(double number)
        {
            byte[] response;

            string strNumber = String.Format("{0:000.000}", number);

            response = Encoding.ASCII.GetBytes(strNumber);

            return response;
        }
        private byte[] coordToFixedBytes(coordinate coord)
        {
            

            byte[] x = floatToFixedBytes(coord.x);
            byte[] y = floatToFixedBytes(coord.y);
            byte[] z = floatToFixedBytes(coord.z);
            byte[] yaw = floatToFixedBytes(coord.yaw);

            byte[] response = new byte[x.Length + y.Length + z.Length + yaw.Length + 3]; //7 bytes per float after formatting, 3 commas seperating

            int indexOffset = 0;

            for(int i = 0; i < x.Length; i++)
            {
                response[i] = x[i];
            }
            response[x.Length] = (byte)',';

            indexOffset = x.Length + 1;

            for (int i = 0; i < y.Length; i++)
            {
                response[indexOffset + i] = y[i];
            }
            response[indexOffset + y.Length] = (byte)',';

            indexOffset += y.Length + 1;

            for (int i = 0; i < z.Length; i++)
            {
                response[indexOffset + i] = z[i];
            }
            response[indexOffset + z.Length] = (byte)',';

            indexOffset += z.Length + 1;

            for (int i = 0; i < yaw.Length; i++)
            {
                response[indexOffset + i] = yaw[i];
            }

            return response;
        }
        private byte[] prependByteArray(byte prependableByte, byte[] byteArray)
        {
            byte[] newArray = new byte[byteArray.Length + 1];
            newArray[0] = prependableByte;
            for(int i = 0; i < byteArray.Length; i++)
            {
                newArray[i + 1] = byteArray[i];
            }
            return newArray;
        }
        private bool UploadParameterToEmbedded(byte parameterCode, byte[] parameter)
        {
            try
            {
                int messageLength = parameter.Length + 2;
                byte[] parameterMsg = new byte[messageLength];

                parameterMsg[0] = 0x08;
                parameterMsg[1] = parameterCode;

                for (int i = 0; i < parameter.Length; i++)
                {
                    parameterMsg[2 + i] = parameter[i];
                }

                return embedded.WriteToEmbeddedSystem(parameterMsg);
            }
            catch
            {
                return false;
            }
        }
        private void UploadParametersBtn_Click(object sender, RoutedEventArgs e)
        {
            bool successfulUpload = true;
            ParametersSuccessfullyUploaded = false;
            string selectedPackageName = PackageTypesCB.SelectedItem.ToString();
            ICPackage selectedIC = ICPackage.LoadFromFile(selectedPackageName);
            string msg = "Uploading " + selectedIC.Name + " parameters to embedded system..";
            Console.WriteLine(msg);
            consoleUpdateModel.WriteLineToConsole("Uploading " + selectedIC.Name + " parameters to embedded system..");

            byte[] bytesForParameter = coordToFixedBytes(selectedIC.PickupLocation);
            if(!UploadParameterToEmbedded(0x81, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload PickupLocation coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.PreheatLocation);
            if (!UploadParameterToEmbedded(0x82, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload PreheatLocation coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.FluxFountainEntryLocation);
            if (!UploadParameterToEmbedded(0x83, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload FluxFountainEntryLocation coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.FluxFountainWettingLocation1);
            if (!UploadParameterToEmbedded(0x84, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload FluxFountainWettingLocation1 coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.FluxFountainWettingLocation2);
            if (!UploadParameterToEmbedded(0x85, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload FluxFountainWettingLocation2 coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.SolderPotEntryLocation);
            if (!UploadParameterToEmbedded(0x86, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload SolderPotEntryLocation coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.SolderPotWettingLocation1);
            if (!UploadParameterToEmbedded(0x87, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload SolderPotWettingLocation1 coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.SolderPotWettingLocation2);
            if (!UploadParameterToEmbedded(0x88, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload SolderPotWettingLocation2 coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = coordToFixedBytes(selectedIC.DropoffLocation);
            if (!UploadParameterToEmbedded(0x89, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload DropoffLocation coord");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = floatToFixedBytes(selectedIC.ClearZHeight);
            if (!UploadParameterToEmbedded(0x8A, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload ClearZHeight");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = floatToFixedBytes(selectedIC.PreheatSoakTime);
            if (!UploadParameterToEmbedded(0x8B, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload PreheatSoakTime");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = floatToFixedBytes(selectedIC.TravelSpeed);
            if (!UploadParameterToEmbedded(0x8C, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload TravelSpeed");
                successfulUpload = false;
            }
            Thread.Sleep(50);
            bytesForParameter = floatToFixedBytes(selectedIC.ActionSpeed);
            if (!UploadParameterToEmbedded(0x8D, bytesForParameter))
            {
                Debug.WriteLine("Failed to upload ActionSpeed");
                successfulUpload = false;
            }
            Thread.Sleep(50);

            if (successfulUpload)
            {
                StartButton.IsEnabled = true;
                ParametersSuccessfullyUploaded = true;
                AICTStateController.SetMachineState(AICTState.MachineState.ReadyToRun);
            }
        }

        private void ModifyOperatorNameButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendDataBtn_Click(object sender, RoutedEventArgs e)
        {
            
            //MessageEmbedded(0x01, "");
            string textToSend = CommandEntryTB.Text.Trim() + "\n";
            embedded.WriteToMotionController(textToSend);
            //MessageEmbedded(0x00, textToSend);
            string returnBuffer = embedded.ReadBufferFromEmbeddedSystem();
            if (returnBuffer.Length != 0)
            {
                consoleUpdateModel.WriteLineToConsole(returnBuffer);
                try
                {
                    string command = CommandEntryTB.Text.Trim();
                    byte[] commandBuffer = new byte[2];

                    commandBuffer[0] = 0x07;//serial passthrough command
                    commandBuffer[1] = 0x01;//enable serial passthrough
                    embedded.WriteToEmbeddedSystem(commandBuffer, 2);

                    commandBuffer = Encoding.ASCII.GetBytes(command);

                    byte[] writeBuffer = new byte[commandBuffer.Length + 2];

                    writeBuffer[0] = 0x07;//serial passthrough
                    writeBuffer[1] = 0x00;//transmit data to motion controller

                    for (int i = 2; i < commandBuffer.Length + 2; i++)
                    {
                        writeBuffer[i] = commandBuffer[i - 2];
                    }

                    embedded.WriteToEmbeddedSystem(writeBuffer, writeBuffer.Length);
                }
                catch (Exception ex)
                {

                }
            }
        }
            void EmbeddedDataPollingBtn_Click(object sender, RoutedEventArgs e)
            {
                if (embeddedFeedbackEnabled)
                {
                    embeddedFeedbackEnabled = false;
                    EmbeddedDataPollingBtn.Content = "Enable Feedback";
                }
                else
                {
                    embeddedFeedbackEnabled = true;
                    EmbeddedDataPollingBtn.Content = "Disable Feedback";
                }
            }

            void ReadFromBufferBtn_Click(object sender, RoutedEventArgs e)
            {
                byte[] buffer = embedded.ReadBuffer();
                consoleUpdateModel.WriteLineToConsole(Encoding.ASCII.GetString(buffer));
            }

        private void HomeMachineBtn_Click(object sender, RoutedEventArgs e)
        {
            embedded.WriteToMotionController("$H\n");
            embedded.WriteToMotionController("G92 X0 Y0 Z0 A0\n");
            embedded.WriteToMotionController("G90\n");
        }
    }
    }

