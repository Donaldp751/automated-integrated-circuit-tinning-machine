﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using System.Security.Cryptography;

namespace NVE_IC_Tinning_Control_Panel
{
    public class EmbeddedCommunication
    {
        bool communicationBusy = false;
        private bool _heartbeatReceived = false;
        byte[] confirmKey = { 0x05, 0xFF, 0xF0, 0x0F, 0x00 };

        private ushort inputWord = 0; //represents the inputs to the embedded system
        private ushort outputWord = 0; //represents the outputs to the embedded system
        private ushort statusWord = 0; //represents the status of the machines operation

        private int readTimeout = 1000;
        private int writeTimeout = 1000;

        byte[] serialReceivedBuffer = new byte[1000];
        bool validEmbeddedConnection;
        string embeddedSerialPort;

        int rxIndex = 0;
        byte[] rxBuffer = new byte[4096];
        List<byte[]> receivedMessages = new List<byte[]>();
        List<byte[]> receivedCmdResponses = new List<byte[]>();
        List<byte[]> machineStatusResponses = new List<byte[]>();

        SerialPort _serialPort;
        public bool IsConnected { get; set; }
        public bool IsHeartbeatReceived
        {
            get
            {
                if (_heartbeatReceived)
                {
                    _heartbeatReceived = false;
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
        }
        Int32 CRC8(byte[] data, int offset, int length)
        {
            Int32 crc = 0x00;
            int extract;
            int sum;
            if(data.Length < length + offset)
            {
                return -1;
            }
            for (int i = offset; i < length + offset; i++)
            {
                extract = data[i];
                for (byte tempI = 8; tempI > 0; tempI--)
                {
                    sum = (crc ^ extract) & 0x01;
                    crc >>= 1;
                    if (sum > 0)
                        crc ^= 0x8C;
                    extract >>= 1;
                }
            }
            return crc;
        }

        Int32 CRC8(byte[] data, int length)
        {
            Int32 crc = 0x00;
            int extract;
            int sum;
            for (int i = 0; i < length; i++)
            {
                extract = data[i];
                for (byte tempI = 8; tempI > 0; tempI--)
                {
                    sum = (crc ^ extract) & 0x01;
                    crc >>= 1;
                    if (sum > 0)
                        crc ^= 0x8C;
                    extract >>= 1;
                }
            }
            return crc;
        }
        public UInt16 GetStatusWord()
        {
            return statusWord;
        }
        public UInt16 GetInputWord()
        {
            return this.inputWord;
        }
        public UInt16 GetOutputWord()
        {
            return this.outputWord;
        }
        public bool StartAutomation()
        {
            if (this.communicationBusy)
            {
                Thread.Sleep(250);
                if (this.communicationBusy)
                {
                    return false;
                }
            }

            if (!this.IsConnected)
            {
                return false;
            }

            try
            {
                byte[] txBytes = new byte[2];
                txBytes[0] = 0x06; //state change code
                txBytes[1] = 0x01; //start automation code
                return this.WriteToEmbeddedSystem(txBytes);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception thrown while trying to start automation..");
                Debug.WriteLine(e);
            }

            return false;
        }
        public bool PauseAutomation()
        {
            if (this.communicationBusy)
            {
                Thread.Sleep(250);
                if (this.communicationBusy)
                {
                    return false;
                }
            }

            if (!this.IsConnected)
            {
                return false;
            }

            try
            {
                byte[] txBytes = new byte[2];
                txBytes[0] = 0x06; //state change code
                txBytes[1] = 0x02; //pause automation code
                return this.WriteToEmbeddedSystem(txBytes);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception thrown while trying to pause automation..");
                Debug.WriteLine(e);
            }

            return false;
        }
        public bool StopAutomation()
        {
            if (this.communicationBusy)
            {
                Thread.Sleep(250);
                if (this.communicationBusy)
                {
                    return false;
                }
            }

            if (!this.IsConnected)
            {
                return false;
            }

            try
            {
                byte[] txBytes = new byte[2];
                txBytes[0] = 0x06; //state change code
                txBytes[1] = 0x03; //stop automation code
                return this.WriteToEmbeddedSystem(txBytes);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception thrown while trying to stop automation..");
                Debug.WriteLine(e);
            }

            return false;
        }
        public bool WriteToEmbeddedSystem(byte[] writeBuffer)
        {
            return WriteToEmbeddedSystem(writeBuffer, writeBuffer.Length);
        }

        public string ReadBufferFromEmbeddedSystem()
        {

            if (!_serialPort.IsOpen)
            {
                return "";
            }
            try
            {
                if (_serialPort.BytesToRead == 0)
                {
                    return "";
                }
                if (communicationBusy)
                {
                    while (communicationBusy) ;
                }
                communicationBusy = true;
                byte[] rxBuffer = new byte[_serialPort.BytesToRead];
                _serialPort.Read(rxBuffer, 0, _serialPort.BytesToRead);
                return Encoding.ASCII.GetString(rxBuffer);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return "";
        }
        public byte[] ReadBuffer()
        {
            int bytesAvailable = _serialPort.BytesToRead;
            byte[] buffer = new byte[bytesAvailable];
            _serialPort.Read(buffer, 0, bytesAvailable);
            return buffer;

        }
        public string ReadResponseFromEmbeddedSystem()
        {
            try
            {
                if (communicationBusy)
                {
                    while (communicationBusy) ;
                }
                communicationBusy = true;
                byte[] rxBuffer = new byte[500];
                Debug.Write("available bytes = " + _serialPort.BytesToRead.ToString());
                int bytesRead = 0;

                int waitCounter = 100;
                while ((_serialPort.BytesToRead < 3) && ((waitCounter--) > 0)) //wait till the data's been received and is available in the buffer, or giveup and move on
                {
                    Thread.Sleep(10);
                    //Console.WriteLine(SerialPort.BytesToRead);
                }

                rxBuffer[bytesRead++] = (byte)_serialPort.ReadByte();
                while (rxBuffer[bytesRead - 1] != 0x03)
                {
                    rxBuffer[bytesRead++] = (byte)_serialPort.ReadByte();
                }

                int startingOffset = 0;

                //rxBuffer[bytesRead++] = (byte)_serialPort.ReadByte();

                for (int i = 0; i < bytesRead; i++)
                {
                    if (rxBuffer[i] == 0x01)
                    {
                        startingOffset = i;
                        break;
                    }
                }

                if (rxBuffer[startingOffset] == 0x01)
                {

                    int messageLength = (rxBuffer[startingOffset + 1] << 8) | rxBuffer[startingOffset + 2];
                    int lenCRC = rxBuffer[startingOffset + 3];

                    if (lenCRC == CRC8(rxBuffer, startingOffset + 1, 2))
                    {
                        //for (int i = 0; i < messageLength; i++)

                        int messageCRC = rxBuffer[startingOffset + 4];

                        waitCounter = 100;
                        while ((_serialPort.BytesToRead < 3) && ((waitCounter--) > 0)) //wait till the data's been received and is available in the buffer, or giveup and move on
                        {
                            Thread.Sleep(10);
                        }

                        rxBuffer = new byte[messageLength];
                        bytesRead = _serialPort.Read(rxBuffer, 0, messageLength);

                        if (bytesRead != messageLength)
                        {
                            Thread.Sleep(100);
                            bytesRead = _serialPort.Read(rxBuffer, bytesRead, messageLength - bytesRead);
                        }

                        _serialPort.ReadByte();//ending byte

                        int calculatedCRC = CRC8(rxBuffer, 0, messageLength);

                        if (messageCRC != calculatedCRC)
                        {
                            Debug.WriteLine("INVALID CRC FOR MESSAGE");
                            communicationBusy = false;
                            return null;
                        }
                        else
                        {
                            byte[] outBuffer = new byte[messageLength];
                            for (int i = 0; i < messageLength; i++)
                            {
                                outBuffer[i] = rxBuffer[i];
                            }
                            communicationBusy = false;
                            return Encoding.ASCII.GetString(outBuffer);
                        }

                    }

                }

                communicationBusy = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return "";
        }
        public bool WriteToMotionController(string message)
        {
            int messageLen = message.Length; //two extra bytes to store the serial passthrough command

            byte[] txBuffer = new byte[messageLen + 2];

            txBuffer[0] = 0x07;//serial passthrough
            txBuffer[1] = 0x00;//transmit to motion controller

            byte[] messageArray = Encoding.ASCII.GetBytes(message);

            for (int i = 0; i < message.Length; i++)
            {
                txBuffer[i + 2] = messageArray[i];
            }

            return WriteToEmbeddedSystem(txBuffer);
        }
        public bool WriteToEmbeddedSystem(byte[] writeBuffer, int length)
        {
            if(_serialPort == null)
            {
                return false;
            }
            if (!_serialPort.IsOpen)
            {
                return false;
            }
            else
            {
                try
                {
                    byte[] txBuffer = new byte[7];
                    byte[] len = new byte[2];

                    if (length > 252)
                    {
                        throw new IndexOutOfRangeException("message too long for transmission");
                    }
                    txBuffer[0] = 0x01; //start of header
                    txBuffer[1] = (byte)((length) >> 8); //length of message high byte, beyond the 5 byte header
                    txBuffer[2] = (byte)((length) & 0xFF); //length of message low byte
                    len[0] = txBuffer[1];
                    len[1] = txBuffer[2];
                    txBuffer[3] = (byte)CRC8(len, 2); //CRC of length of message 
                    txBuffer[4] = (byte)CRC8(writeBuffer, length); //CRC of message
                    txBuffer[5] = 0x03; //End of header
                    txBuffer[6] = 0x04; //End of transmission (sent after the data)

                    if (communicationBusy)
                    {
                        for (int i = 0; i < 10; i++) //wait up to 500 ms for communication to become not busy
                        {
                            Thread.Sleep(50);
                            if (!communicationBusy)
                            {
                                break;
                            }
                        }
                    }

                    if (communicationBusy)
                    {
                        throw new TimeoutException("Unable to write to embedded device, communication busy beyond timeout period");
                    }

                    communicationBusy = true; //lock to write to serial port

                    _serialPort.Write(txBuffer, 0, 6); //write header to embedded device
                    _serialPort.Write(writeBuffer, 0, length); //write message data 
                    _serialPort.Write(txBuffer, 6, 1); //write EOT 

                    communicationBusy = false; //unlock when completed successfully

                }
                catch (TimeoutException e)
                {
                    try
                    {
                        _serialPort.Close();
                        communicationBusy = false;
                        return false;
                    }
                    catch
                    {
                        return false;
                    }

                }
                catch (Exception e)
                {
                    communicationBusy = false; //unlock when completed unsuccessfully too
                    Debug.WriteLine(e);
                    try
                    {
                        byte[] cancelCode = { 0x18 };
                        _serialPort.Write(cancelCode, 0, 1); //tell the embedded device to clear rx buffer, if for sure a write fails or an exception is thrown while writing. 

                    }
                    catch
                    {
                        communicationBusy = false;
                        Debug.WriteLine("Failed to write cancel code to embedded device");
                    }
                }


            }


            return true;
        }

        bool checkMSG(byte[] msg)
        {
            if(msg[0] == 0x01)
            {
                UInt16 msgLen = (UInt16)((msg[1] << 8) | (msg[2]));

                int crc = CRC8(msg, 1,2); //length CRC

                if (msg[3] == crc)
                {
                    crc = CRC8(msg, 6, msgLen); //msg CRC
                    if(crc == msg[4])
                    {
                        return true;//valid CRC
                    }
                    else
                    {
                        Debug.WriteLine("MSG CRC Invalid");
                    }
                }
                else
                {
                    Debug.WriteLine("MSG Len CRC Invalid");
                }
            }

            return false;
        }
        public string getLatestMessageContents()
        {
            if (receivedMessages.Count != 0)
            {
                byte[] msg = receivedMessages.First<byte[]>();
                receivedMessages.Remove(msg);
                if (checkMSG(msg))
                {
                    if (msg[6] != 0x06)
                    {
                        byte[] strippedMSG = new byte[msg.Length];
                        int newLength = 0;

                        for (int i = 7; i < msg.Length; i++) //Start past the header
                        {
                            if (((msg[i] < 32) | (msg[i] > 126)) && !((msg[i] == 10) | (msg[i] == 13)))
                            {
                                byte character = msg[i]; //need to handle commands that are out of range with normal messages
                            }
                            else
                            {
                                strippedMSG[newLength++] = msg[i];
                            }
                        }
                        return Encoding.ASCII.GetString(strippedMSG);
                    }
                    else
                    {//embedded response, not console output.
                        
                    }
                }
                else
                {
                    Debug.WriteLine("Message check not valid..");
                    Debug.WriteLine(Encoding.ASCII.GetString(msg));
                }
            }
            return null;
        }
        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int bytesToRead = _serialPort.BytesToRead;
            int startOfMessageOffset;
            for (int i = 0; i < bytesToRead; i++)
            {
                int rx = _serialPort.ReadByte();
                if (rx == -1)
                { //end of stream

                }
                else
                {
                    rxBuffer[rxIndex++] = (byte)rx;
                    
                    if ((rx == 0x04) && (rxIndex > 5))
                    {
                        int SOH = 0;
                        for(int z = 0; z < rxIndex-8; z++)
                        {
                            if(rxBuffer[z] == 0x01)
                            {
                                byte[] tempBuffer = new byte[rxIndex - SOH];
                                for(int j = 0; j < tempBuffer.Length; j++)
                                {
                                    tempBuffer[j] = rxBuffer[j + SOH];
                                }
                                if ((tempBuffer[z + 6] == 0x09))
                                {
                                    if ((tempBuffer[z + 7] == 0x23)) //contains I/O data
                                    {
                                        _heartbeatReceived = true;
                                        inputWord = ((UInt16)((((tempBuffer[z + 7 + 1] << 8) | (tempBuffer[z + 7 + 2]))^0xf0f0) & UInt16.MaxValue));
                                        outputWord = ((UInt16)((((tempBuffer[z + 7 + 3] << 8) | (tempBuffer[z + 7 + 4]))^0xf0f0) & UInt16.MaxValue));
                                        statusWord  = ((UInt16)((((tempBuffer[z + 7 + 5] << 8) | (tempBuffer[z + 6 + 4]))^ 0xf0f0) & UInt16.MaxValue));
                                        communicationBusy = false;
                                        IsConnected = true;
                                    }
                                    else if ((tempBuffer[z + 7] == 0x22)) //regular heartbeat
                                    {
                                        _heartbeatReceived = true;
                                    }
                                }
                                else if ((tempBuffer[z + 6] == 0x21)) //message for console
                                {
                                    _heartbeatReceived = true;
                                    receivedMessages.Add(tempBuffer);
                                }

                                
                                rxIndex = 0;
                                break;
                            }
                        }
                    }
                }
            }
        }

        public string ConnectToEmbeddedSystemPort()
        {
            string[] ports = SerialPort.GetPortNames();
            byte[] crcBuffer = new byte[100];
            bool CRCValid = false;

            string validPort = null;
            if (ports.Length == 0) return null;
            foreach (string port in ports)
            {
                try
                {
                    _serialPort = new SerialPort(port, 115200, Parity.None, 8, StopBits.One);
                    _serialPort.NewLine = "\n";
                    _serialPort.ReadTimeout = readTimeout;
                    _serialPort.WriteTimeout = writeTimeout;
                    _serialPort.Open();

                    Thread.Sleep(50);

                    if (!_serialPort.IsOpen)
                    {
                        continue;
                    }

                    if (!WriteToEmbeddedSystem(confirmKey, 5))
                    {
                        continue;
                    }



                    //_serialPort.Write(confirmKey, 0, 4);
                    //Console.WriteLine(SerialPort.BytesToRead); //spam available bytes 
                    int waitCounter = 100;
                    while ((_serialPort.BytesToRead < 3) && ((waitCounter--) > 0)) //wait till the data's been received and is available in the buffer, or giveup and move on
                    {
                        Thread.Sleep(10);
                        //Console.WriteLine(SerialPort.BytesToRead);
                    }
                    Console.WriteLine(waitCounter);
                    Console.WriteLine(_serialPort.BytesToRead);
                    if (_serialPort.BytesToRead < 4)
                    {
                        Console.WriteLine("No response from " + port);
                        _serialPort.Close();
                        continue;
                    }
                    int receivedBytes = _serialPort.Read(serialReceivedBuffer, 0, 6);

                    if (serialReceivedBuffer[0] == 0x01)
                    {
                        crcBuffer[0] = serialReceivedBuffer[1];
                        crcBuffer[1] = serialReceivedBuffer[2];
                        int crc = CRC8(crcBuffer, 2);
                        if (serialReceivedBuffer[3] == crc)
                        {
                            CRCValid = true;
                        }
                    }

                    if (CRCValid)
                    {
                        receivedBytes = _serialPort.Read(serialReceivedBuffer, 6, 6);
                    }




                    validEmbeddedConnection = true;

                    for (int i = 1; i < 5; i++)
                    {
                        if (serialReceivedBuffer[i + 6] != confirmKey[i])
                        {
                            validEmbeddedConnection = false;
                        }
                    }
                    //Console.WriteLine("Connection successful!");

                    if (validEmbeddedConnection)
                    {
                        Console.WriteLine("Embedded Connection confirmed");
                        _serialPort.DataReceived += _serialPort_DataReceived;
                        validPort = port;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Embedded device not found on " + port);
                    }

                    Console.WriteLine(_serialPort.ReadLine());

                }
                catch (TimeoutException e)
                {
                    _serialPort.Close();
                    Debug.WriteLine(e);
                    continue;
                }
                catch (Exception e)
                {
                    _serialPort.Close();
                    Debug.WriteLine(e);
                    validEmbeddedConnection = false;
                }

            }

            if (validPort != null)
            {
                embeddedSerialPort = validPort;
            }

            return validPort;
        }
        public bool TransmitHeartbeat(bool requestGPIOStatus)
        {
            try
            {
                byte[] txBuffer = {0x09, 0x22 }; //SYN IDLE;
                if (requestGPIOStatus)
                {
                    txBuffer[1] = 0x23; //Heartbeat, and get status words
                }
                return WriteToEmbeddedSystem(txBuffer);
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool Heartbeat(bool getGPIOStatus)
        {
            try
            {
                
                if (communicationBusy)
                {
                    IsConnected = true;
                    return true;
                }

                if (_serialPort == null)
                {
                    //this.ConnectToEmbeddedSystemPort();
                    IsConnected = false;
                    return false;
                }
                if (_serialPort.IsOpen && validEmbeddedConnection)
                {
                    if (communicationBusy)
                    {
                        int comWaitCounter = 200;
                        while (communicationBusy)
                        {
                            Thread.Sleep(10);
                            comWaitCounter--;
                            if (comWaitCounter == 0)
                            {
                                throw new TimeoutException("embedded connection busy too long while waiting to check heartbeat");
                            }
                        }
                    }
                    communicationBusy = true;
                    _serialPort.DataReceived -= _serialPort_DataReceived;

                    byte[] txBuffer = { 0x22 }; //SYN IDLE;
                    if (getGPIOStatus)
                    {
                        txBuffer[0] = 0x23; //Heartbeat, and get status words
                    }
                    _serialPort.Write(txBuffer, 0, 1);
                    //Console.WriteLine(_serialPort.BytesToRead);

                    int responseTimeoutCounter = 10;
                    while (_serialPort.BytesToRead < 1)
                    {
                        responseTimeoutCounter--;
                        Thread.Sleep(50);
                        if (responseTimeoutCounter == 0)
                        {
                            IsConnected = false;
                            communicationBusy = false;
                            _serialPort.DataReceived += _serialPort_DataReceived;
                            return false;
                        }
                    }

                    int receivedBytes = _serialPort.Read(serialReceivedBuffer, 0, 1);

                    if (receivedBytes > 0)
                    {

                        byte byteReceived = serialReceivedBuffer[0];
                        if (byteReceived == 0x22) //Normal everything is ok heartbeat
                        {
                            IsConnected = true;
                            communicationBusy = false;
                            _serialPort.DataReceived += _serialPort_DataReceived;
                            return true;
                        }
                        else if (byteReceived == 0x23) //heartbeat followed by sensor and machine states.
                        {
                            _serialPort.Read(serialReceivedBuffer, 1, 4);

                            inputWord = (UInt16)((serialReceivedBuffer[1] << 8) | (serialReceivedBuffer[2]));
                            outputWord = (UInt16)((serialReceivedBuffer[3] << 8) | (serialReceivedBuffer[4]));
                            communicationBusy = false;
                            IsConnected = true;
                            _serialPort.DataReceived += _serialPort_DataReceived;
                            return true;
                        }
                        else if (byteReceived == 0x24) //Operation currently paused 
                        {
                            IsConnected = true;
                            communicationBusy = false;
                            _serialPort.DataReceived += _serialPort_DataReceived;
                            return true;
                        }
                        else if (byteReceived == 0x25) //Operation halted, and will need to be reset 
                        {
                            IsConnected = true;
                            communicationBusy = false;
                            _serialPort.DataReceived += _serialPort_DataReceived;
                            return true;
                        }

                    }
                }
            }
            catch (NullReferenceException e)
            {
                Debug.WriteLine("No Valid Com port found");
            }
            catch (TimeoutException e)
            {
                Debug.WriteLine(e);
                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception thrown during heartbeat");
                Debug.WriteLine(e);
            }
            communicationBusy = false;
            IsConnected = false;
            _serialPort.DataReceived += _serialPort_DataReceived;
            return false;
        }
        public EmbeddedCommunication()
        {

        }
        public EmbeddedCommunication(string portName)
        {
            validEmbeddedConnection = false;

            string[] ports = SerialPort.GetPortNames();
            if (ports.Length == 0)
            {
                ErrorWindow errorWindow = new ErrorWindow();
                errorWindow.ErrorWindowTitleTB.Text = "No COM devices available";
                errorWindow.ErrorTB.Text = "No COM ports available, attempted to connect to port:" + portName + " but device does not exist";
            }
            foreach (string port in ports)
            {
                if (String.Equals(port, portName))
                {
                    try
                    {
                        _serialPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                        _serialPort.NewLine = "\n";
                        _serialPort.ReadTimeout = 500;
                        _serialPort.WriteTimeout = 100;

                        _serialPort.Open();
                        _serialPort.Write(confirmKey, 0, 4);
                        Console.WriteLine(_serialPort.BytesToRead);
                        while (_serialPort.BytesToRead < 3)
                        {

                        }
                        int receivedBytes = _serialPort.Read(serialReceivedBuffer, 0, 4);


                        validEmbeddedConnection = true;

                        for (int i = 0; i < 4; i++)
                        {
                            if (serialReceivedBuffer[i] != confirmKey[i])
                            {
                                validEmbeddedConnection = false;
                            }
                        }
                        if (validEmbeddedConnection)
                        {
                            Console.WriteLine("Embedded Connection confirmed");
                        }
                        Console.WriteLine("Connection successful!");
                        Console.WriteLine(_serialPort.ReadLine());

                    }
                    catch (Exception e)
                    {
                        _serialPort.Close();
                        validEmbeddedConnection = false;
                        ErrorWindow errorWindow = new ErrorWindow();
                        errorWindow.ErrorWindowTitleTB.Text = "Failed to connect to: " + port;
                        errorWindow.ErrorTB.Text = e.Message;

                    }
                }
            }
        }
        void CloseConnection()
        {

            try
            {
                _serialPort.DataReceived -= _serialPort_DataReceived;
                _serialPort.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception on closing serial port");
                Debug.WriteLine(e);
            }


        }
        ~EmbeddedCommunication()
        {
            if (_serialPort != null && _serialPort.IsOpen)
            {
                CloseConnection();
            }
        }
    }
}