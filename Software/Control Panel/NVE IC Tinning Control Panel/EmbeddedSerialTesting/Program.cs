﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NVE_IC_Tinning_Control_Panel;

namespace EmbeddedSerialTesting
{
    class Program
    {
        
        static void Main(string[] args)
        {
            EmbeddedCommunication embeddedCommunication = new EmbeddedCommunication();

            embeddedCommunication.ConnectToEmbeddedSystemPort();

            Console.WriteLine(embeddedCommunication.Heartbeat(true));

            Console.WriteLine(embeddedCommunication.GetInputWord());
            Console.WriteLine(embeddedCommunication.GetOutputWord());

            embeddedCommunication.WriteToMotionController("$$\n");

            Thread.Sleep(1500);

            string msg = embeddedCommunication.getLatestMessageContents();

            if(msg != null)
            {
                Console.WriteLine(msg);
            }

            

            

            //embeddedCommunication.WriteToMotionController("")

            string inputBuffer = "";

            /*
            while (!inputBuffer.Contains("q"))
            {
                inputBuffer = Console.ReadLine();
            }
            */
            Console.ReadKey();
        }
    }
}
