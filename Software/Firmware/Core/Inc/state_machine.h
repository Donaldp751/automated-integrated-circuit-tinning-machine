/*
 * state_machine.h
 *
 *  Created on: Jun 18, 2020
 *      Author: dposterick
 */

#ifndef INC_STATE_MACHINE_H_
#define INC_STATE_MACHINE_H_

#include "main.h"

#define standbyMasterFlashingFreq 1
#define runningMasterFlashingFreq 5
#define startupMasterFlashingFreq 10
#define errorStatusFlashingFreq 7
#define stoppingMasterFlashingFreq 3

extern uint8_t automationCommandsReady;
extern uint8_t startAutomation;
extern uint8_t pauseAutomation;
extern uint8_t stopAutomation;
extern uint8_t machineHomed;
extern uint8_t requestPaused;

enum MACHINE_STATES{
	init,
	standby,
	disconnected,
	reconnected,
	pause,
	startup,
	running,
	error,
	emergencyStopped,
	stopping
};

#define Machine_States enum MACHINE_STATES

struct MachineStates{
	Machine_States currentState;
	Machine_States previousState;
	Machine_States preErrorState;
};

#define States struct MachineStates

extern States AICTState;
/*enum Run_States{

};*/

void nextLoop();

#endif /* INC_STATE_MACHINE_H_ */
