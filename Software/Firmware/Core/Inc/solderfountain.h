/*
 * fluxfountain.h
 *
 *  Created on: Sep 8, 2020
 *      Author: dposterick
 */

#ifndef INC_SOLDERFOUNTAIN_H_
#define INC_SOLDERFOUNTAIN_H_


int sf_setup(void);


int sf_start(void);
void sf_start_off(void);
int sf_stop(void);
void sf_stop_off(void);

#endif /* INC_SOLDERFOUNTAIN_H_ */
