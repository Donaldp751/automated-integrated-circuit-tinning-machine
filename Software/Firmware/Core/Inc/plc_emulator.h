#ifndef __PLC_EMULATOR_H__
#define __PLC_EMULATOR_H__


#include <stdint.h>


int plce_time_update(uint32_t time_ms);
int plce_add_callback(uint32_t time_from_now_ms, void (*function_to_call)(void));
int plce_cleanup();
#endif
