/*
 * float_formatting.h
 *
 *  Created on: Sep 16, 2020
 *      Author: dposterick
 */

#ifndef INC_FLOAT_FORMATTING_H_
#define INC_FLOAT_FORMATTING_H_

int fmt_fp(char *outstr, double y, int w, int p, int fl);

#endif /* INC_FLOAT_FORMATTING_H_ */
