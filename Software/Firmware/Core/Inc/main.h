/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define heartbeatTimeoutms 5000
#define automationLoopFreq 200 // 48Mhz clock,9600 counter period, 4 prescaler,  (48,000,000 / 9600) / 4 = 1250 hz on timer 3
#define heartbeatTimeoutCounter ( heartbeatTimeoutms * automationLoopFreq )

extern volatile uint8_t heartbeatReceived;

extern volatile uint16_t inputWord;
extern volatile uint16_t outputWord;
extern volatile uint16_t machineStatusWord;

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;


#define MS_MOTIONACTIVE 0
#define MS_RUNNING 1
#define MS_ERROR 2
#define MS_STOPPING 3
#define MS_PAUSED 4
#define MS_EMTPYINPUT 5
#define MS_FULLOUTPUT 6
#define MS_RESV1 7
#define MS_RESV2 8
#define MS_RESV3 9
#define MS_STEP 12 //11-15 reserved for IC Plating step

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MASTER_STATUS_LED_Pin GPIO_PIN_13
#define MASTER_STATUS_LED_GPIO_Port GPIOC
#define ERROR_LED_Pin GPIO_PIN_14
#define ERROR_LED_GPIO_Port GPIOC
#define DIAGNOSTIC_LED_Pin GPIO_PIN_15
#define DIAGNOSTIC_LED_GPIO_Port GPIOC
#define D_IN0_Pin GPIO_PIN_0
#define D_IN0_GPIO_Port GPIOA
#define D_IN0_EXTI_IRQn EXTI0_IRQn
#define D_IN1_Pin GPIO_PIN_1
#define D_IN1_GPIO_Port GPIOA
#define D_IN2_Pin GPIO_PIN_2
#define D_IN2_GPIO_Port GPIOA
#define D_IN2_EXTI_IRQn EXTI2_IRQn
#define D_IN3_Pin GPIO_PIN_3
#define D_IN3_GPIO_Port GPIOA
#define D_IN4_Pin GPIO_PIN_4
#define D_IN4_GPIO_Port GPIOA
#define D_IN5_Pin GPIO_PIN_5
#define D_IN5_GPIO_Port GPIOA
#define D_IN6_Pin GPIO_PIN_6
#define D_IN6_GPIO_Port GPIOA
#define D_IN7_Pin GPIO_PIN_7
#define D_IN7_GPIO_Port GPIOA
#define D_OUT0_Pin GPIO_PIN_0
#define D_OUT0_GPIO_Port GPIOB
#define D_OUT1_Pin GPIO_PIN_1
#define D_OUT1_GPIO_Port GPIOB
#define D_OUT7_Pin GPIO_PIN_12
#define D_OUT7_GPIO_Port GPIOB
#define D_OUT8_Pin GPIO_PIN_13
#define D_OUT8_GPIO_Port GPIOB
#define D_OUT9_Pin GPIO_PIN_14
#define D_OUT9_GPIO_Port GPIOB
#define D_OUT10_Pin GPIO_PIN_15
#define D_OUT10_GPIO_Port GPIOB
#define D_IN8_Pin GPIO_PIN_8
#define D_IN8_GPIO_Port GPIOA
#define Master_TX_Pin GPIO_PIN_9
#define Master_TX_GPIO_Port GPIOA
#define Master_RX_Pin GPIO_PIN_10
#define Master_RX_GPIO_Port GPIOA
#define D_OUT2_Pin GPIO_PIN_5
#define D_OUT2_GPIO_Port GPIOB
#define D_OUT3_Pin GPIO_PIN_6
#define D_OUT3_GPIO_Port GPIOB
#define D_OUT4_Pin GPIO_PIN_7
#define D_OUT4_GPIO_Port GPIOB
#define D_OUT5_Pin GPIO_PIN_8
#define D_OUT5_GPIO_Port GPIOB
#define D_OUT6_Pin GPIO_PIN_9
#define D_OUT6_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

//output defines
//mixed voltage output selectable after 7

//input track
#define PORT_UPPER_SOLENOID D_OUT0_GPIO_Port
#define PIN_UPPER_SOLENOID D_OUT0_Pin
#define PORT_LOWER_SOLENOID D_OUT1_GPIO_Port
#define PIN_LOWER_SOLENOID D_OUT1_Pin

// preheater
#define PORT_PREHEATER D_OUT2_GPIO_Port
#define PIN_PREHEATER D_OUT2_Pin

// output track solenoid air assist
#define PORT_OUTPUTTRACK_AIR D_OUT3_GPIO_Port
#define PIN_OUTPUTTRACK_AIR D_OUT3_Pin

//solder fountain start pulse
#define PIN_SOLDER_FOUNTAIN_START D_OUT4_Pin
#define PORT_SOLDER_FOUNTAIN_START D_OUT4_GPIO_Port

//solder fountain stop pulse
#define PIN_SOLDER_FOUNTAIN_STOP D_OUT5_Pin
#define PORT_SOLDER_FOUNTAIN_STOP D_OUT5_GPIO_Port

//flux fountain
#define PORT_FLUX_FOUNTAIN D_OUT6_GPIO_Port
#define PIN_FLUX_FOUNTAIN D_OUT6_Pin


//inputs

#define PORT_PICKUP_PART D_IN2_GPIO_Port
#define PIN_PICKUP_PART D_IN2_Pin

#define PORT_PART_READY_INPUT D_IN1_GPIO_Port
#define PIN_PART_READY_INPUT D_IN1_Pin

#define PORT_OUTPUTTRACK_CLEARED D_IN0_GPIO_Port
#define PIN_OUTPUTTRACK_CLEARED D_IN0_Pin

#define PORT_OUTPUTTRACK_FULL D_IN0_GPIO_Port
#define PIN_OUTPUTTRACK_FULL D_IN0_Pin






/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
