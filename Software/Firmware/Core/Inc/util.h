/*
 * util.h
 *
 *  Created on: Aug 12, 2020
 *      Author: dposterick
 */

#ifndef INC_UTIL_H_
#define INC_UTIL_H_

#include "main.h"
#include "state_machine.h"
#include "IC_Plating.h"

int parseDataFromHost(uint8_t *startingAddress);
int writeToHost(uint8_t *startingAddress, int len);
uint8_t CRC8(uint8_t *data, int length);
int reconnect(void);
int writeMsgToHost(uint8_t *startingAddress, int len);
int parseMsgFromMotion(char *buffer, int len);
void requestMotionStatusUpdate(void);
extern uint8_t enableSerialPassthrough;


#endif /* INC_UTIL_H_ */
