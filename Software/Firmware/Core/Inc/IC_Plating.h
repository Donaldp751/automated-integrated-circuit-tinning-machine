/*
 * IC_Plating.h
 *
 *  Created on: Aug 17, 2020
 *      Author: donny
 */

#ifndef INC_IC_PLATING_H_
#define INC_IC_PLATING_H_

#include "main.h"
#include "state_machine.h"

void MainAutomation(uint8_t inhibitAction, Machine_States machineState);

typedef struct _Coordinate{
	float x,y,z,yaw;
} Coordinate;

enum _IC_Plating_Process{
	waitForStart, //wait till the operator starts
	moveAbovePickupLocation, //Sends motion commands, wait not required
	waitForPart,
	moveToPreheatLocation, //move the pickhead above the preheat location
	preheatIC, //lower part to preheat location
	moveToFluxEntryLocation, //move to the entry location
	applyFluxToLeads, //move to the first flux location, then back, rotate, and flux the otherside of the part
	moveToSolderPotEntryLocation, //move the IC above the solder pot to the entry position for tinning
	applySolderToLeads, //tinning process
	moveToDropoffLocation, //move to the output track
	releasePart, //drop the IC
	waitForOutputPart, //wait till the part passes the output sensor
	stopIfFull, //stop Processing parts if the output is full
	stop //stop immediately, turn off equipment
};
#define IC_Plating_Process enum _IC_Plating_Process

extern IC_Plating_Process currentStep;


struct settings_s
{
	Coordinate PickupLocation;
	uint8_t PickupLocationSet;

	Coordinate PreheatLocation;
	uint8_t PreheatLocationSet;

	Coordinate FluxFountainEntryLocation;
	uint8_t FluxFountainEntryLocationSet;

	Coordinate FluxFountainWettingLocation1;
	uint8_t FluxFountainWettingLocation1Set;

	Coordinate FluxFountainWettingLocation2;
	uint8_t FluxFountainWettingLocation2Set;

	Coordinate SolderPotEntryLocation;
	uint8_t SolderPotEntryLocationSet;

	Coordinate SolderPotWettingLocation1;
	uint8_t SolderPotWettingLocation1Set;

	Coordinate SolderPotWettingLocation2;
	uint8_t SolderPotWettingLocation2Set;

	Coordinate DropoffLocation;
	uint8_t DropoffLocationSet;

	float ClearZHeight;
	uint8_t ClearZHeightSet;

	int PreheatSoakTime;
	uint8_t PreheatSoakTimeSet;

	int TravelSpeed;
	uint8_t TravelSpeedSet;

	int ActionSpeed;
	uint8_t ActionSpeedSet;
};

extern struct settings_s global_settings;


int settings_clear(struct settings_s * s);
int settings_ready(struct settings_s * s);


#endif /* INC_IC_PLATING_H_ */
