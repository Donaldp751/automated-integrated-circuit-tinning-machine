/*
 * output_track.h
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#ifndef INC_OUTPUT_TRACK_H_
#define INC_OUTPUT_TRACK_H_



int ot_setup(void);

void ot_assist_part_off(void);
int ot_assist_part(void);
int ot_is_full(void);

// "part detection"
int ot_wait_for_part(void);
int ot_part_detected(void);
int ot_part_passed(void);

#endif /* INC_OUTPUT_TRACK_H_ */
