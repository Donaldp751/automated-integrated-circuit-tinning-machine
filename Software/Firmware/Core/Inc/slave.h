/*
 * slave.h
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#ifndef INC_SLAVE_H_
#define INC_SLAVE_H_

#include "IC_Plating.h"

int	slave_movement_goto_pickup(void);
int	slave_movement_goto_preheat(void);
int	slave_movement_goto_flux(void);
int	slave_movement_apply_flux(void);
int	slave_movement_goto_solder(void);
int	slave_movement_apply_solder(void);
int	slave_movement_goto_dropoff(void);
int slave_move_clear_z(void);
int slave_vacuum_enable(void);
int slave_vacuum_disable(void);
int slave_send_motion_command(Coordinate * location, uint16_t speed);

int SendEmergencyStopCommand();
int SendCycleStartCommand();
int SendHomingCycleCommand();
int SendFeedHoldCommand();
int SendClearBufferCommand();

int slave_is_moving(void);

void slave_fb_is_moving(void);
void slave_fb_is_idle(void);

//void SendMotionCommand(Coordinate * location, uint16_t speed);
//void SendVacuumCommand(uint16_t on);
uint32_t CalculateDuration_ms(Coordinate * l1, Coordinate* l2, uint16_t speed);



#endif /* INC_SLAVE_H_ */
