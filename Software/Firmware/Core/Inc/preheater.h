/*
 * preheater.h
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#ifndef INC_PREHEATER_H_
#define INC_PREHEATER_H_



int preheater_setup(void);

int preheater_start(void);
int preheater_stop(void);

int preheater_start_timer(void);
void preheater_completed_heating(void);
int preheater_check_completed(void);

#endif /* INC_PREHEATER_H_ */
