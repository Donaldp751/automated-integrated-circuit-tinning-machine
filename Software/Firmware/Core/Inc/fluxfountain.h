/*
 * fluxfountain.h
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#ifndef INC_FLUXFOUNTAIN_H_
#define INC_FLUXFOUNTAIN_H_



int ff_setup(void);


int ff_start(void);
int ff_stop(void);
void ff_stop_cb(void);

#endif /* INC_FLUXFOUNTAIN_H_ */
