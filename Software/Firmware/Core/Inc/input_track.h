/*
 * input_track.h
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#ifndef INC_INPUT_TRACK_H_
#define INC_INPUT_TRACK_H_


int it_setup(void);

// "controls"
int it_start_loop(void); // when user presses start
int it_stop_loop(void);  // when user presses stop

// "outputs"
int it_pickup_ready(void);   // is part available for pickup
int it_parts_on_track(void); // are there more parts above the solenoids

// "part detection"
int it_wait_for_part(void);
int it_part_detected(void);
int it_is_part_ready(void);


#endif /* INC_INPUT_TRACK_H_ */
