/*
 * state_machine.c
 *
 *  Created on: Jun 18, 2020
 *      Author: dposterick
 */

#include "state_machine.h"
#include "preheater.h"
#include "input_track.h"
#include "solderfountain.h"
#include "fluxfountain.h"
#include "IC_Plating.h"
#include "main.h"
#include "slave.h"

	uint8_t ERROR_DETECTED = 0;
	uint8_t PAUSE_REQUIRED = 0;

	uint8_t heartbeat = 0;
	uint64_t heartbeatCounter = 0;

	uint32_t nextTransitionTime = 0;
	uint16_t initTime = 2000;
	uint16_t startupTime = 20000;
	//uint16_t runningTime = 10000;
	uint16_t stoppingTime = 2000;

	uint8_t automationCommandsReady = 0;
	uint8_t startAutomation = 0;
	uint8_t pauseAutomation = 0;
	uint8_t stopAutomation = 0;
	uint8_t requestPaused = 1;
	uint8_t machineHomed = 0;

	States AICTState;


void signals(){
	 uint32_t tickTimer = 0;
	 tickTimer = HAL_GetTick();
	 int8_t flasherMasterSolid = 0;
	 int8_t flasherMasterFreq = 0;
	 int8_t flasherErrorFreq = 0;

	 switch(AICTState.currentState){
 	 	 case init:

 	     break;

 	 	 case standby:
 	 		flasherMasterSolid = 1;
 	 		flasherMasterFreq =1;
 	 		flasherErrorFreq = -1;
		 break;

 	 	 case pause:

 	     break;

 	 	 case disconnected:
  	 		flasherMasterSolid = 0;
  	 		flasherMasterFreq = 10;
  	 		flasherErrorFreq = 5;
 	     break;

	 	 case reconnected:
	  	 	flasherMasterSolid = 0;
	  	 	flasherMasterFreq = 10;
	  	 	flasherErrorFreq = -1;
	 	 break;

 	 	 case startup:
 	 		flasherMasterSolid = 0;
  	 		flasherMasterFreq = startupMasterFlashingFreq;
  	 		flasherErrorFreq = -1;
 	     break;

 	 	 case running:
 	 		flasherMasterSolid = 0;
  	 		flasherMasterFreq = runningMasterFlashingFreq;
  	 		flasherErrorFreq = -1;
 	     break;

 	 	 case error:
 	 		flasherMasterSolid = 0;
  	 		flasherMasterFreq = 1;
  	 		flasherErrorFreq = errorStatusFlashingFreq;
 	 	 break;

 	 	 case stopping:
 	 		flasherMasterSolid = 0;
  	 		flasherMasterFreq = standbyMasterFlashingFreq;
  	 		flasherErrorFreq = -1;
 	 	 break;

 	 	 case emergencyStopped:
  	 		flasherMasterSolid = 0;
   	 		flasherMasterFreq = 20;
   	 		flasherErrorFreq = 20;
 	 	 break;
	 }

	 //if(current == standby)
	 if(flasherMasterFreq){
		 if(((tickTimer % (1000/flasherMasterFreq)) == 0)){
		 	 HAL_GPIO_TogglePin(MASTER_STATUS_LED_GPIO_Port, MASTER_STATUS_LED_Pin);
	 	 }
	 }else{
		 if(flasherMasterSolid){
		 		 	 HAL_GPIO_WritePin(MASTER_STATUS_LED_GPIO_Port, MASTER_STATUS_LED_Pin, GPIO_PIN_SET);
		 	 	 }
	 }

	 if(flasherErrorFreq){
		 if(flasherErrorFreq & ((tickTimer % (1000/flasherErrorFreq)) == 0)){
			 HAL_GPIO_TogglePin(ERROR_LED_GPIO_Port, ERROR_LED_Pin);
		 }else if(flasherErrorFreq == -1){
			 HAL_GPIO_WritePin(ERROR_LED_GPIO_Port, ERROR_LED_Pin, GPIO_PIN_RESET);
		 	 }
	 	 }
	}


void updateState(Machine_States nextState){
	AICTState.previousState = AICTState.currentState;
	AICTState.currentState = nextState;
}

void transistion(){
	uint8_t inhibitAutomation = 0;

	 switch(AICTState.currentState){
	 	 case init:

	 		updateState(standby);
	     break;

	 	 case standby:
	 		 automationCommandsReady = settings_ready(&global_settings);
	 		 if(startAutomation && automationCommandsReady){
	 			 requestPaused = 1;
	 			 SendCycleStartCommand();
	 			 SendHomingCycleCommand();
	 			 preheater_start();
	 			 startAutomation &= 0;
	 			 stopAutomation &= 0;
	 			 pauseAutomation &= 0;
	 			 updateState(startup);
	 		 }
		 break;

	 	 case disconnected:
		 	AICTState.previousState = AICTState.currentState;
		 	inhibitAutomation = 1;
		 	if(heartbeat){
		 		updateState(AICTState.preErrorState);
		 	}
	 	 break;

	 	 case reconnected:

	 	 break;

	 	 case startup:
	 		 if(AICTState.previousState != AICTState.currentState){
	 			nextTransitionTime = HAL_GetTick() + startupTime;
	 		 }
	 		AICTState.previousState = AICTState.currentState;
	 		 if(HAL_GetTick() >= nextTransitionTime){

	 			AICTState.currentState = running;
	 			requestPaused = 0;
	 		 }

	     break;

	 	 case running:
	 		AICTState.previousState = AICTState.currentState;
	 		inhibitAutomation = 0;
	 		if(!heartbeat){
	 			  if(AICTState.currentState != disconnected){
	 				  AICTState.preErrorState = AICTState.currentState;
	 				  AICTState.currentState = disconnected;
	 			  }
	 		}else if(ERROR_DETECTED){
	 			AICTState.preErrorState = AICTState.currentState;
	 			AICTState.currentState = error;
	 		 }else if(pauseAutomation){
	 			SendFeedHoldCommand();
	 			 pauseAutomation &= 0;
	 			 updateState(pause);
	 		 }else if(stopAutomation){
	 	 		 SendFeedHoldCommand();
	 			 stopAutomation &= 0;
	 			 updateState(stopping);
	 		 }
	     break;

	 	 case error:

	 	 break;

	 	 case pause:
	 		 inhibitAutomation = 1;
	 		 if(stopAutomation){
	 			stopAutomation &= 0;
	 			updateState(stopping);
	 		 }
	 		 if(startAutomation){
	 			 SendCycleStartCommand();
	 			 updateState(running);
	 		 }
	 	 break;

	 	 case emergencyStopped:
	 		SendFeedHoldCommand();
	 		SendClearBufferCommand();
		 	preheater_stop();
		 	sf_stop();

	 	 break;


	 	 case stopping:
	 		if(AICTState.previousState != AICTState.currentState){
	 			updateState(stop);
	 		}else{
	 			updateState(standby);
	 		}

	 		AICTState.previousState = AICTState.currentState;
	 	 break;
	 }
	 MainAutomation(inhibitAutomation, AICTState.currentState);
}



void timing(){
	  heartbeat = heartbeatCounter < heartbeatTimeoutCounter; //when the device has not been connected for

	  if(!heartbeatReceived){
			  heartbeatCounter++;
	  }else{
			  heartbeatCounter = 0;
		  }

}


uint8_t firstLoop = 1;
void nextLoop(){
	if(firstLoop){
		firstLoop = 0;
		AICTState.currentState = init;
	}
	signals(); //status LED's and slow flashing signals
	transistion();
	timing();
}
