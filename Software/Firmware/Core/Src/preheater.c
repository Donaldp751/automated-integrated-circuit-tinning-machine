/*
 * preheater.c
 *
 *  Created on: Sep 3, 2020
 *      Author: dhuang
 */


#include "preheater.h"
#include "plc_emulator.h"
#include "main.h"
#include "IC_Plating.h"

int preheater_setup(void)
{
	preheater_stop();
	return 0;
}

int preheater_start(void)
{
	HAL_GPIO_WritePin(PORT_PREHEATER, PIN_PREHEATER, GPIO_PIN_SET);
	return 0;

}

int preheater_stop(void)
{
	HAL_GPIO_WritePin(PORT_PREHEATER, PIN_PREHEATER, GPIO_PIN_RESET);
	return 0;


}

uint8_t heatingComplete = 0;

int preheater_start_timer(void){
	heatingComplete = 0;
	plce_add_callback(global_settings.PreheatSoakTime, preheater_completed_heating);
	return 0;
}

void preheater_completed_heating(void){
	heatingComplete = 1;
}

int preheater_check_completed(void){
	return heatingComplete;
}
