/*
 * input_track.c
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#include "plc_emulator.h"
#include "input_track.h"
#include "main.h"

#define OPEN GPIO_PIN_SET
#define CLOSE GPIO_PIN_RESET


enum it_state_e
{
IT_S1, // all closed, part exists between solenoids
IT_S2, // lower is open, part doesn't exist between solenoids
IT_S3, // all closed, part doesn't exist between solenoids
IT_S4, // upper is open, part exists between solenoids
};


static int it_started = 0;
static int it_state = IT_S3;
static int it_part_singulated = 0;
static int it_part_ready_pickup = 0;


void it_superloop(void);



int it_upper_solenoid(int newstate)
{
	HAL_GPIO_WritePin(PORT_UPPER_SOLENOID, PIN_UPPER_SOLENOID, newstate);
	return 0;
}

int it_lower_solenoid(int newstate)
{
	HAL_GPIO_WritePin(PORT_LOWER_SOLENOID, PIN_LOWER_SOLENOID, newstate);
	return 0;
}

//
int it_setup(void)
{
	it_upper_solenoid(CLOSE);
	it_lower_solenoid(CLOSE);
	return 0;
}

// "controls"
int it_start_loop(void)
{
	it_state = IT_S3;
	it_started = 1;
	plce_add_callback(100, &it_superloop);
	return 0;
}


int it_stop_loop(void)
{
	it_started = 0;
	return 0;
}

int it_wait_for_part(void){
	it_part_singulated = 1;
	return 0;
}

int it_part_detected(void){
	if(it_part_singulated){
		it_part_ready_pickup = 1;
		it_part_singulated = 0;
		HAL_GPIO_WritePin(D_OUT9_GPIO_Port, D_OUT9_Pin, GPIO_PIN_SET);
	}
	return 0;
}

int it_is_part_ready(void){
	if(it_part_ready_pickup){
		HAL_GPIO_WritePin(D_OUT9_GPIO_Port, D_OUT9_Pin, GPIO_PIN_RESET);
		it_part_ready_pickup = 0;
		return 1;
	}else{
		return 0;
	}
}

void it_superloop(void)
{
	if(it_started == 0)
		return;
   switch(it_state)
   {
   case IT_S1:
	   //if(!it_pickup_ready())
	   //{
		   it_lower_solenoid(OPEN);
		   it_state = IT_S2;
	   //}
	   break;
   case IT_S2:
	   it_lower_solenoid(CLOSE);
	   it_state = IT_S3;
	   it_wait_for_part();
	   return; //dont repeat, just singulate a single part down
	   break;
   case IT_S3:
	   //if(it_parts_on_track())
	   //{
	   	   it_part_ready_pickup = 0;
		   it_upper_solenoid(OPEN);
		   it_state = IT_S4;
	   //}
	   break;
   case IT_S4:
	   it_upper_solenoid(CLOSE);
	   it_state = IT_S1;
	   break;
   }

   plce_add_callback(200, &it_superloop);
}

// "outputs"
int it_pickup_ready(void)
{
	return it_is_part_ready();
	//return HAL_GPIO_ReadPin(PORT_PICKUP_PART, PIN_PICKUP_PART) == GPIO_PIN_RESET;
}


int it_parts_on_track(void)
{
	return HAL_GPIO_ReadPin(PORT_PART_READY_INPUT, PIN_PART_READY_INPUT);
}
