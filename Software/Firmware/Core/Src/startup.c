#include "main.h"

void flashOutput(GPIO_TypeDef *gpioPort, uint16_t pin, uint16_t freq, uint16_t count){
	for(int i = 0; i < count; i++){
	HAL_GPIO_WritePin(gpioPort, pin, GPIO_PIN_RESET);
	HAL_Delay((1000/freq));
	HAL_GPIO_WritePin(gpioPort, pin, GPIO_PIN_SET);
	HAL_Delay((1000/freq));
	}
	HAL_GPIO_WritePin(gpioPort, pin, GPIO_PIN_RESET);
}

void startupSequence(){
	  for(int i = 0; i<10;i++){
	  HAL_GPIO_TogglePin(MASTER_STATUS_LED_GPIO_Port, MASTER_STATUS_LED_Pin);
	  HAL_GPIO_TogglePin(DIAGNOSTIC_LED_GPIO_Port, DIAGNOSTIC_LED_Pin);
	  HAL_GPIO_TogglePin(ERROR_LED_GPIO_Port, ERROR_LED_Pin);
	  HAL_Delay(250);
	  }

	  flashOutput(D_OUT0_GPIO_Port,D_OUT0_Pin, 3, 3);
	  flashOutput(D_OUT1_GPIO_Port,D_OUT1_Pin, 3, 3);
	  flashOutput(D_OUT2_GPIO_Port,D_OUT2_Pin, 3, 3);
	  flashOutput(D_OUT3_GPIO_Port,D_OUT3_Pin, 3, 3);



}
