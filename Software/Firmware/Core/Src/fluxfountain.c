/*
 * fluxfountain.c
 *
 *  Created on: Sep 3, 2020
 *      Author: dhuang
 */


#include "fluxfountain.h"
#include "main.h"
#include "plc_emulator.h"

int ff_setup(void)
{
	ff_stop();
	return 0;
}

int ff_start(void)
{
	HAL_GPIO_WritePin(PORT_FLUX_FOUNTAIN, PIN_FLUX_FOUNTAIN, GPIO_PIN_SET);
	return 0;
}


int ff_stop(void)
{
	plce_add_callback(1000, ff_stop_cb);
	return 0;
}

void ff_stop_cb(void){
	HAL_GPIO_WritePin(PORT_FLUX_FOUNTAIN, PIN_FLUX_FOUNTAIN, GPIO_PIN_RESET);
}
