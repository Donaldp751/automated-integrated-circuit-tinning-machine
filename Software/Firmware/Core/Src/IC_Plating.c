/*
 * IC_Plating.c
 *
 *  Created on: Aug 17, 2020
 *      Author: donny
 */

#include <stdio.h>
#include "IC_Plating.h"
#include "output_track.h"
#include "slave.h"
#include "preheater.h"
#include "fluxfountain.h"
#include "solderfountain.h"
#include "input_track.h"
#include "util.h"
#include "string.h"
#include "plc_emulator.h"


IC_Plating_Process currentStep = waitForStart;

uint16_t currentAutomationStepIndex = 0;
uint16_t previousAutomationStepIndex = 0;

struct settings_s global_settings = {0};


int TravelSpeed = 100; // while moving between operations mm/s
uint8_t TravelSpeedSet = 0;

int ActionSpeed = 10; // while performing critical actions mm/s
uint8_t ActionSpeedSet = 0;

uint8_t mainStateChanged(){
	return AICTState.previousState != AICTState.currentState;
}

/*
void reportState(IC_Plating_Process step){
	char buffer[100];
	uint32_t currentTick = HAL_GetTick();
	return;

	switch(currentStep){
		case waitForStart: //wait till the operator starts
			sprintf(buffer,"Waiting to start... t=%lu\n",currentTick);
			break;
		case moveAbovePickupLocation:
			sprintf(buffer, "Moving to the pickup location. t=%lu\n",currentTick);
			break;
		case waitForPart:
			sprintf(buffer, "Waiting for the part. t=%lu\n",currentTick);
			break;
		case moveToPreheatLocation: //move the pickhead above the preheat location
			sprintf(buffer, "Moving the part to the preheat location. t=%lu\n",currentTick);
			break;
		case preheatIC:  //lower part to preheat location
			sprintf(buffer, "Lowering the part to preheat. t=%lu\n",currentTick);
			break;
		case moveToFluxEntryLocation: //move to the flux entry location
			sprintf(buffer, "Moving the part to the flux fountain entry location. t=%lu\n",currentTick);
			break;
		case applyFluxToLeads: //move to the first flux location, then back, rotate, and flux the otherside of the part
			sprintf(buffer, "Applying flux to leads. t=%lu\n",currentTick);
			break;
		case moveToSolderPotEntryLocation: //move the IC above the solder pot to the entry position for tinning
			sprintf(buffer, "Moving to solder pot entry location. t=%lu\n",currentTick);
			break;
		case applySolderToLeads: //move the IC above the solder pot to the entry position for tinning
			sprintf(buffer, "Applying solder to IC leads. t=%lu\n",currentTick);
			break;
		case moveToDropoffLocation: //move to the output track
			sprintf(buffer, "Moving to the dropoff location. t=%lu\n",currentTick);
			break;
		case releasePart:  //drop the IC
			sprintf(buffer, "Releasing the part. t=%lu\n",currentTick);
			break;
		case stopIfFull:  //drop the IC
			sprintf(buffer, "Output full, waiting till empty before continuing. t=%lu\n",currentTick);
			break;
		case stop: //turn off everything and go back to standby
			sprintf(buffer, "Halting automation, going back to standby. t=%lu\n",currentTick);
			break;
		}
	writeMsgToHost((uint8_t*)buffer, strlen(buffer));
}
*/

void MainAutomation(uint8_t inhibitAction, Machine_States machineState){
	if(inhibitAction){
		return;
	}

	if(machineState == standby)currentStep = waitForStart;

	machineStatusWord &= ~(0xF000);
	machineStatusWord |= (currentStep)<<MS_STEP;

	switch(currentStep){
	case waitForStart: //wait till the operator starts

		if(mainStateChanged()){
			if(AICTState.previousState == startup && AICTState.currentState == running){
				currentStep = moveAbovePickupLocation;
				slave_movement_goto_pickup();
			}
		}
		break;

	case moveAbovePickupLocation:
		if(machineState == stopping){
					currentStep = standby;
				}
		if(!slave_is_moving())
		{
			currentStep = waitForPart;
			it_start_loop();
		}
		break;
	case waitForPart:

		if(it_pickup_ready())
		{
			slave_vacuum_enable();
			slave_movement_goto_preheat();
			currentStep = moveToPreheatLocation;
		}
		break;
	case moveToPreheatLocation: //move the pickhead above the preheat location
		if(!slave_is_moving())
		{
			currentStep = preheatIC;
			ff_start();
			preheater_start_timer();
		}
		break;

	case preheatIC:  //lower part to preheat location
		if(preheater_check_completed())
		{
			currentStep = moveToFluxEntryLocation;
			slave_movement_goto_flux();

		}
		break;
	case moveToFluxEntryLocation: //move to the flux entry location
		if(!slave_is_moving())
		{
			currentStep = applyFluxToLeads;
			slave_movement_apply_flux();
		}
		break;
	case applyFluxToLeads: //move to the first flux location, then back, rotate, and flux the otherside of the part
		if(!slave_is_moving())
		{
			currentStep = moveToSolderPotEntryLocation;
			ff_stop();
			sf_start();
			slave_movement_goto_solder();
		}
		break;
	case moveToSolderPotEntryLocation: //move the IC above the solder pot to the entry position for tinning
		if(!slave_is_moving())
		{
			currentStep = applySolderToLeads;
			slave_movement_apply_solder();
		}
		break;
	case applySolderToLeads: //move the IC above the solder pot to the entry position for tinning
		if(!slave_is_moving())
		{
			currentStep = moveToDropoffLocation;
			slave_movement_goto_dropoff();
			sf_stop();
		}
		break;
	case moveToDropoffLocation: //move to the output track
		if(!slave_is_moving())
		{
			currentStep = releasePart;
		}
		break;
	case releasePart:  //drop the IC
		if(ot_is_full())
		{
			currentStep = stopIfFull;
		}
		else if(machineState == stopping){
			currentStep = stop;
		}
		else
		{
			// release part
			slave_vacuum_disable();
			currentStep = waitForOutputPart;
			//it_start_loop();
			ot_assist_part();
			slave_movement_goto_pickup();
		}
		break;

	case waitForOutputPart:
		if(ot_part_passed()){
			currentStep = moveAbovePickupLocation;
		}

		break;
	case stopIfFull:  //drop the IC
		preheater_stop();
		if(!ot_is_full()){ //After the output has been emptied, continue
			currentStep = releasePart;
		}
		if(machineState == stopping){
			currentStep = standby;
		}
		break;
	case stop: //turn off everything and go back to standby
		preheater_stop();
		slave_vacuum_disable();
		it_stop_loop();
		sf_stop();
		slave_move_clear_z();
		plce_cleanup();
		currentStep = waitForStart;
		break;
	}
}



int settings_clear(struct settings_s * s)
{
	memset(s, 0, sizeof(struct settings_s));
	return 0;
}

int settings_ready(struct settings_s * s)
{
	if(!s->PickupLocationSet)
		return 0;
	if(!s->PreheatLocationSet)
		return 0;
	if(!s->FluxFountainEntryLocationSet)
		return 0;
	if(!s->FluxFountainWettingLocation1Set)
		return 0;
	if(!s->FluxFountainWettingLocation2Set)
		return 0;
	if(!s->SolderPotEntryLocationSet)
		return 0;
	if(!s->SolderPotWettingLocation1Set)
		return 0;
	if(!s->SolderPotWettingLocation2Set)
		return 0;
	if(!s->DropoffLocationSet)
		return 0;
	if(!s->ClearZHeightSet)
		return 0;
	if(!s->PreheatSoakTimeSet)
		return 0;
	if(!s->TravelSpeedSet)
		return 0;
	if(!s->ActionSpeedSet)
		return 0;


	return 1;
}
