/*
 * ouput_track.c
 *
 *  Created on: Sep 3, 2020
 *      Author: dhuang
 */


#include "main.h"
#include "output_track.h"
#include "plc_emulator.h"
#include "IC_Plating.h"

#define OUTPUT_TRACK_AIR_ON_TIME_MS 1000

static int ot_part_singulated = 0;
static int ot_part_ready_pickup = 0;

int ot_air(uint16_t newstatus)
{
	HAL_GPIO_WritePin(PORT_OUTPUTTRACK_AIR, PIN_OUTPUTTRACK_AIR, newstatus);
	return 0;
}

int ot_setup(void)
{
	ot_assist_part_off();
	return 0;
}

void ot_assist_part_off(void)
{
	ot_air(GPIO_PIN_RESET);
}

int ot_assist_part(void)
{
	ot_wait_for_part(); //get ready for part to be output
	ot_air(GPIO_PIN_SET);
	plce_add_callback(OUTPUT_TRACK_AIR_ON_TIME_MS,ot_assist_part_off );
	return 0;
}


int ot_is_full(void)
{
	return HAL_GPIO_ReadPin(PORT_OUTPUTTRACK_FULL, PIN_OUTPUTTRACK_FULL) == GPIO_PIN_SET;
}

int ot_wait_for_part(void){
	ot_part_singulated = 1;
	return 0;
}

int ot_part_detected(void){
	if(ot_part_singulated){
		ot_part_ready_pickup = 1;
		ot_part_singulated = 0;
		HAL_GPIO_WritePin(D_OUT10_GPIO_Port, D_OUT10_Pin, GPIO_PIN_SET);
	}
	return 0;
}

int ot_part_passed(void){
	if(ot_part_ready_pickup){
		HAL_GPIO_WritePin(D_OUT10_GPIO_Port, D_OUT10_Pin, GPIO_PIN_RESET);
	}
	return ot_part_ready_pickup;
}
