/*
 * interrupts.c
 *
 *  Created on: Aug 12, 2020
 *      Author: dposterick
 */
#include "main.h"
#include "util.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern uint8_t connectedToHost;
extern uint8_t receivingCommands;
extern uint8_t rx3End;
extern uint8_t *huart3RXBuffer; //Buffer for data received from the host over RS232

extern uint8_t rx1End;
extern uint8_t *huart1RXBuffer; //Buffer for data received from the motion controller over TTL
extern TIM_HandleTypeDef htim4;



