/*
 * slave.c
 *
 *  Created on: Sep 2, 2020
 *      Author: dhuang
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "slave.h"
#include "IC_Plating.h"
#include "util.h"
#include "main.h"
#include "state_machine.h"
#include "float_formatting.h"

//int gcvt(float, int, char *buffer);

uint32_t estimated_slave_finish = 0;
uint32_t prehold_estimated_slave_finish = 0;
uint32_t currently_moving = 0;
uint8_t currently_moving_fb = 0;
Coordinate last_location = {0};

#define MX 1
#define MY 2
#define MZ 4
#define MA 8

int gcvt(float x, int prec, char *buffer){
	fmt_fp(buffer, x, 0, prec, 0);
	return 0;
}

int slave_setlocation(Coordinate * location, int current_location)
{
	memcpy(&last_location, location, sizeof (Coordinate));
	if (current_location)
	{
		currently_moving = 0;
		estimated_slave_finish = 0;

	}
	return 0;
}


void slave_move(Coordinate * location, uint16_t speed, uint16_t axis_mask)
{
	uint32_t finish_time;
	Coordinate new_location;

	memcpy(&new_location, &last_location, sizeof(Coordinate));

	if(axis_mask & MX)
	{
		new_location.x = location->x;
	}
	if(axis_mask & MY)
	{
		new_location.y = location->y;
	}
	if(axis_mask & MZ)
	{
		new_location.z = location->z;
	}
	if(axis_mask & MA)
	{
		new_location.yaw = location->yaw;
	}

	finish_time = CalculateDuration_ms(&last_location, &new_location, speed);

	if(!slave_is_moving())
	{
		estimated_slave_finish = HAL_GetTick() + finish_time;
	}
	else
	{
		estimated_slave_finish += finish_time;
	}

	slave_send_motion_command(&new_location, speed);
	currently_moving = 1;

	slave_setlocation(&new_location, 0);
}



void slave_fb_is_moving(){
	currently_moving_fb = 1;
}
void slave_fb_is_idle(){
	currently_moving_fb = 0;
}

int slave_is_moving(void) // no reason to bothing updating currently_moving unless this explicitely gets called
{

	if(currently_moving)
	{
		uint32_t currentTick = HAL_GetTick();
		if(currentTick > estimated_slave_finish)
		{
			currently_moving = 0;
		}
	}
	machineStatusWord &= ~(1<<MS_MOTIONACTIVE);
	machineStatusWord |= (!(currently_moving_fb==0)) & (1<<MS_MOTIONACTIVE);

	return currently_moving |  currently_moving_fb;
}

int slave_move_clear_z(){
	Coordinate new_location;

	memcpy(&new_location, &last_location, sizeof(Coordinate));
	new_location.z = global_settings.ClearZHeight;

	slave_move(&new_location,global_settings.TravelSpeed, MZ);

	return 0;
}

// movement commands

int	slave_movement_goto_pickup(void)
{
	slave_move_clear_z(); //raise the Z above
	slave_move(&global_settings.PickupLocation, global_settings.TravelSpeed, MX | MY | MA); //move the X, and Y above the pickup location, and rotate the pickhead to the correct angle for pickup
	slave_move(&global_settings.PickupLocation, global_settings.TravelSpeed, MZ);
	return 0;
}

int	slave_movement_goto_preheat(void)
{

	slave_move_clear_z(); //raise the Z above
	slave_move(&global_settings.PreheatLocation, global_settings.TravelSpeed, MX | MY); //move to above the preheat location x,y
	slave_move(&global_settings.PreheatLocation, global_settings.TravelSpeed, MZ); //lower the pickhead to the preheat location z

	return 0;
}

int	slave_movement_goto_flux(void)
{
	//slave_move_clear_z(); //raise the Z above
	slave_move(&global_settings.FluxFountainEntryLocation, global_settings.TravelSpeed, MX | MY | MZ);
	//slave_move(&global_settings.FluxFountainEntryLocation, global_settings.TravelSpeed, MZ);

	return 0;
}

int	slave_movement_apply_flux(void) //assumes already in flux fountain
{
	slave_move(&global_settings.FluxFountainWettingLocation1, global_settings.ActionSpeed, MA); //rotate to first side
	slave_move(&global_settings.FluxFountainWettingLocation1, global_settings.ActionSpeed, MX | MY); //move first set of leads into flux fountain
	slave_move(&global_settings.FluxFountainWettingLocation1, global_settings.ActionSpeed, MZ); //Lower into flux fountain

	slave_move(&global_settings.FluxFountainEntryLocation, global_settings.ActionSpeed, MZ); //move out of flux fountain

	slave_move(&global_settings.FluxFountainWettingLocation2, global_settings.ActionSpeed, MA); //rotate to second side
	slave_move(&global_settings.FluxFountainWettingLocation2, global_settings.ActionSpeed, MX | MY); //move second set of leads into flux fountain
	slave_move(&global_settings.FluxFountainWettingLocation2, global_settings.ActionSpeed, MZ); //lower into flux fountain


	slave_move(&global_settings.FluxFountainEntryLocation, global_settings.ActionSpeed, MZ); //move out of flux fountain
	slave_move(&global_settings.FluxFountainEntryLocation, global_settings.ActionSpeed, MX | MY); //move out of flux fountain

	return 0;
}

int	slave_movement_goto_solder(void)
{
	slave_move_clear_z(); //raise the Z above
	slave_move(&global_settings.SolderPotEntryLocation, global_settings.TravelSpeed, MX | MY); //move above solder fountain
	slave_move(&global_settings.SolderPotEntryLocation, global_settings.TravelSpeed, MZ); //move into solder fountain entry location

	return 0;
}

int	slave_movement_apply_solder(void) //assumes already in solder entry position
{
	Coordinate outCoord = global_settings.SolderPotEntryLocation;
	outCoord.yaw = global_settings.SolderPotWettingLocation2.yaw;

	//slave_move(&global_settings.SolderPotWettingLocation1, global_settings.ActionSpeed, MA); //Rotate to first side
	slave_move(&global_settings.SolderPotWettingLocation1, global_settings.ActionSpeed, MX | MY); //Apply solder to first side
	slave_move(&outCoord, global_settings.ActionSpeed, MX | MY | MA); //rotate out to entry location to wick solder from shorting

	//slave_move(&global_settings.SolderPotWettingLocation2, global_settings.ActionSpeed, MA); //Rotate to second side
	slave_move(&global_settings.SolderPotWettingLocation2, global_settings.ActionSpeed, MX | MY); //Apply solder to second side
	slave_move(&global_settings.SolderPotEntryLocation, global_settings.ActionSpeed, MX | MY | MA); //rotate out to entry location to wick solder from shorting

	return 0;
}

int	slave_movement_goto_dropoff(void)
{
	slave_move_clear_z(); //raise the Z above
	slave_move(&(global_settings.DropoffLocation), global_settings.TravelSpeed, MX | MY | MA); //move above dropoff location
	slave_move(&(global_settings.DropoffLocation), global_settings.TravelSpeed, MZ); //move into dropoff position

	return 0;
}

uint8_t motionCommandBuffer[300] = {0}; //used as a buffer for transmitting commands to the motion controller

//speed is in mm/s
int slave_send_motion_command(Coordinate * location, uint16_t speed){

	for(int i = 0; i < 300; i++){
		motionCommandBuffer[i] = 0;
	}


	float x = location->x;
	float y = location->y;
	float z = location->z;
	float yaw = location->yaw;

	int bufIndex = 0;

	motionCommandBuffer[bufIndex++] = 'G';
	motionCommandBuffer[bufIndex++] = '1';
	motionCommandBuffer[bufIndex++] = ' ';
	motionCommandBuffer[bufIndex++] = 'X';

	char coordBuf[100];

	gcvt(x, 3, coordBuf);
	int bufLen = strlen(coordBuf);

	for(int i = 0; i < bufLen; i++){
		motionCommandBuffer[bufIndex++] = coordBuf[i];
	}

	motionCommandBuffer[bufIndex++] = ' ';
	motionCommandBuffer[bufIndex++] = 'Y';

	gcvt(y, 3, coordBuf);
	bufLen = strlen(coordBuf);

	for(int i = 0; i < bufLen; i++){
		motionCommandBuffer[bufIndex++] = coordBuf[i];
	}

	motionCommandBuffer[bufIndex++] = ' ';
	motionCommandBuffer[bufIndex++] = 'Z';

	gcvt(z, 3, coordBuf);
	bufLen = strlen(coordBuf);

	for(int i = 0; i < bufLen; i++){
		motionCommandBuffer[bufIndex++] = coordBuf[i];
	}

	motionCommandBuffer[bufIndex++] = ' ';
	motionCommandBuffer[bufIndex++] = 'A';

	gcvt(yaw, 3, coordBuf);
	bufLen = strlen(coordBuf);

	for(int i = 0; i < bufLen; i++){
		motionCommandBuffer[bufIndex++] = coordBuf[i];
	}

	motionCommandBuffer[bufIndex++] = ' ';
	motionCommandBuffer[bufIndex++] = 'F';

	itoa(speed * 60, coordBuf,  10); //convert mm/s to mm/min
	bufLen = strlen(coordBuf);

	for(int i = 0; i < bufLen; i++){
		motionCommandBuffer[bufIndex++] = coordBuf[i];
	}

	motionCommandBuffer[bufIndex++] = '\n';
	motionCommandBuffer[bufIndex++] = '\r';
/*
	gcvt(x, 6, motionCommandBuffer+4);

	motionCommandBuffer[11] = ' ';
	motionCommandBuffer[12] = 'Y';
	gcvt(y, 6, motionCommandBuffer+13);
	motionCommandBuffer[20] = ' ';
	motionCommandBuffer[21] = 'Z';
	gcvt(z, 6, motionCommandBuffer+22);
	motionCommandBuffer[29] = ' ';
	motionCommandBuffer[30] = 'F';
	itoa(speed, motionCommandBuffer+31, 10);

	int len = strlen((uint8_t*)motionCommandBuffer);
*/

			/*


*/
	//int commandLength = sprintf((char*)motionCommandBuffer, "G1 X%f Y%f Z%f F%u\n\r",location->x,location->y,location->z,speed);
	//int commandLength = asprintf(&strPtr, "G1 X%.5f Y%.5f Z%.5f F%u\n\r",x,y,z,s);
	writeMsgToHost((uint8_t*)motionCommandBuffer, bufIndex); //write motion commands to the console for debugging

	int status = HAL_UART_Transmit(&huart1, motionCommandBuffer, bufIndex,400);
	if(status == 2){
		huart1.gState = HAL_UART_STATE_READY;
		status = HAL_UART_Transmit(&huart1, motionCommandBuffer, bufIndex, 400);
	}
	//free(strPtr);
	//
	return status;
	//return HAL_UART_Transmit_IT(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}



int SendEmergencyStopCommand(){
	int commandLength = 2;
	motionCommandBuffer[0] = 0x24; //ctrl-x or ascii CANCEL
	motionCommandBuffer[1] = '\n';
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_Transmit_IT(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}

int HAL_UART_TX_RETRY(UART_HandleTypeDef *huart, uint8_t *buffer, int cmdlen){
	if(HAL_UART_Transmit(huart, buffer, cmdlen,400) != HAL_OK){
			huart->gState = HAL_UART_STATE_READY;
			return HAL_UART_Transmit(huart, buffer, cmdlen, 400);
		}
	return HAL_OK;
}

int SendCycleStartCommand(){
	int commandLength = sprintf((char*)motionCommandBuffer, "~\n\r");

	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength);
	/*if(HAL_UART_Transmit(&huart1, motionCommandBuffer, commandLength,400) != HAL_OK){
		huart1.gState = HAL_UART_STATE_READY;
		HAL_UART_Transmit(&huart1, motionCommandBuffer, commandLength, 400);
	}*/
	return 0;
}

int SendHomingCycleCommand(){
	Coordinate homeLocation;
	homeLocation.x = 0;
	homeLocation.y = 0;
	homeLocation.z = 0;
	homeLocation.yaw = 0;
	slave_setlocation(&homeLocation, 0);

	machineHomed = 1;

	//int commandLength = sprintf((char*)motionCommandBuffer, "$H\n\rG92 X0 Y0 Z0 A0\n\r");
	int commandLength = sprintf((char*)motionCommandBuffer, "$H\n\rG10 L20 P1 X0 Y0 Z0 A0\n\rG54\n\r");
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}

int SendFeedHoldCommand(){
	int commandLength = sprintf((char*)motionCommandBuffer, "!\n\r");
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}

int SendClearBufferCommand(){
	int commandLength = sprintf((char*)motionCommandBuffer, "!\n\r");
	motionCommandBuffer[0] = 0x18; //ctrl-x clears the buffer
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}

int slave_vacuum_enable(){
	int commandLength = 0;
	commandLength = sprintf((char*)motionCommandBuffer, "M03\n\r");
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}

int slave_vacuum_disable(){
	int commandLength = 0;
	commandLength = sprintf((char*)motionCommandBuffer, "M05\n\r");
	writeMsgToHost((uint8_t*)motionCommandBuffer, commandLength); //write motion commands to the console for debugging
	return HAL_UART_TX_RETRY(&huart1, motionCommandBuffer, commandLength) == HAL_OK;
}


float diffSquared(float p1, float p2){
	float diff = p1 - p2;
	return diff * diff;
}

uint32_t CalculateDuration_ms(Coordinate * l1, Coordinate* l2, uint16_t speed)
{
	float distanceToTravel = 0;

	distanceToTravel = sqrt(diffSquared(l1->x, l2->x) + diffSquared(l1->y, l2->y) + diffSquared(l1->yaw, l2->yaw)) + sqrt(diffSquared(l1->z, l2->z)); //distance to travel in mm

	float msToTravel = ((distanceToTravel / speed) * 1000)+2000;

	return msToTravel;
}
