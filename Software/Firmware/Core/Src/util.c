/*
 * util.c
 *
 *  Created on: Aug 12, 2020
 *      Author: dposterick
 */

#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include "IC_Plating.h"
#include <math.h>
#include "main.h"
#include "slave.h"

extern uint8_t connectedToHost;
extern uint8_t receivingCommands;

extern uint8_t rx3Start;
extern uint8_t rx3End;
extern uint8_t *huart3RXBuffer; //Buffer for data received from the host over RS232

extern uint8_t rx1Start;
extern uint8_t rx1End;
extern uint8_t *huart1RXBuffer; //Buffer for data received from the motion controller over TTL

uint8_t enableSerialPassthrough = 1;
uint8_t heartbeatByte[10] = {0x09, 0x22, 0x09, 0x23}; //extra space for input/output words if needed

uint8_t confirmKey[] = {0x05, 0xFF, 0xF0, 0x0F, 0x00 };

int reconnect(){
	uint8_t connectedToComputer = 0;
	 for(int i = 0; i < rx3End; i++){
		 connectedToComputer = 0;
		 for(int e = 0; e < 4; e++){
			 if(confirmKey[i+e] == huart3RXBuffer[i+e]){
				 connectedToComputer++;
				 if(connectedToComputer == 4){
					 HAL_UART_Transmit_IT(&huart3, confirmKey, 4);
					 HAL_Delay(10);
					 rx3End = 0;
					 connectedToHost = 1;
					 connectedToComputer = 0;
					 break;
				 }
			 }
		 }
	 }
	 if(rx3End == 0){
		 connectedToComputer = 0; //no data received from serial
	 }
	 if(connectedToHost){
		 //printf(connectionMessage);
		 //HAL_UART_Transmit_IT(&huart3, connectionMessage, strlen(connectionMessage));
	 }
	 return connectedToHost;
}

uint8_t CRC8(uint8_t *data, int length)
{
	uint8_t crc = 0x00;
	uint8_t extract;
	uint8_t sum;
   for(int i=0;i<length;i++)
   {
      extract = *data;
      for (uint8_t tempI = 8; tempI; tempI--)
      {
         sum = (crc ^ extract) & 0x01;
         crc >>= 1;
         if (sum)
            crc ^= 0x8C;
         extract >>= 1;
      }
      data++;
   }
   return crc;
}

enum transmissionCodes{
	SOT = 0x01,
	EOH = 0x03,
	EOT = 0x04,
	CANCEL = 0x18
};


uint8_t txBuffer[500];
uint8_t txMsgBuffer[500];

int writeMsgToHost(uint8_t *startingAddress, int len){
	txMsgBuffer[0] = 0x21;
	for(int i = 0; i < len; i++)
		txMsgBuffer[i+1] = startingAddress[i];
	return writeToHost(txMsgBuffer, len+1);
}

int writeToHost(uint8_t *startingAddress, int len){
	txBuffer[0] = 0x01;
	txBuffer[1] = len>>8;
	txBuffer[2] = len & 0xFF;
	txBuffer[3] = CRC8(txBuffer+1, 2);
	txBuffer[4] = CRC8(startingAddress, len);
	txBuffer[5] = 0x03;

	for(int i = 0; i < len; i++){
		txBuffer[i+6] = startingAddress[i];
	}

	txBuffer[6 + len] = 0x04;

	return HAL_UART_Transmit_IT(&huart3, txBuffer, len + 7) == HAL_OK;
}
uint8_t *nextComma(uint8_t *startingAddress){
	uint8_t maxLen = 254;
	if(*startingAddress == ','){
		return startingAddress;
	}
	while(*(++startingAddress) != ','){
		maxLen--;
		if(maxLen == 0)return (uint8_t *)-1;
	}
	return startingAddress;
}

void parseInt(int *numberPlaceholder, uint8_t *startingAddress){
	*numberPlaceholder = strtol((char*)startingAddress, NULL,10);
}
void parseFloat(float *numberPlaceholder, uint8_t *startingAddress){
	*numberPlaceholder = strtof((char*)startingAddress, NULL);
}
uint8_t parseCoordinate(Coordinate *coord, uint8_t *startingAddress, int len){
	char* pEnd;
	char* pStart = (char*)startingAddress;
	//uint8_t *endPtr = nextComma(startingAddress);
	coord->x = strtof(pStart,&pEnd);
	pStart = pEnd + 1;

	coord->y = strtof(pStart, &pEnd);
	pStart = pEnd + 1;

	coord->z = strtof(pStart, &pEnd);
	pStart = pEnd + 1;

	coord->yaw = strtof(pStart, &pEnd);
	pStart = pEnd + 1;

	return 0;
}

//Parses to the end of line
int parseDataFromHost(uint8_t startingAddress[]){
	uint8_t hostConnection = 1;

	//uint8_t *index = startingAddress; //move index past the start of transmission

	if(*(startingAddress) != SOT){
		return -1; //invalid start of header
	}

	uint16_t numberOfBytes = (*(startingAddress+1))<<8 | *(startingAddress+2); //second byte is number of bytes contained in message
	uint8_t crcForLength = *(startingAddress + 3); //third byte is the CRC for the number of bytes
	uint8_t crcForMessage = *(startingAddress + 4);//fourth byte is the CRC for the rest of the message

	if(*(startingAddress+5) != EOH){
		return -3; //invalid header
	}

	uint8_t crc = CRC8(startingAddress+1, 2);

	if(crcForLength != crc){
		//HAL_UART_Transmit_IT(&huart3, huart3RXBuffer + rx3End, 1);
		return -4; //invalid length received
	}

	crc = CRC8((startingAddress+6), numberOfBytes);
	if(crcForMessage != crc){
		return -5; //data error
	}

	switch(*(startingAddress+6)){

	case 0x03: //data transmission

    break;

	case 0x04: //should never be used, reserved

	break;

	case 0x05: //Embedded key confirm
		hostConnection = 1;
		for(int i = 0; i < 4; i++){
			if(confirmKey[i] != *(i + (startingAddress+6))){
				hostConnection = 0;
			}
		}
		if(hostConnection){
			connectedToHost = 1;
			writeToHost(confirmKey, 5);
		}
	break;

	case 0x06://State change command
		switch(*(startingAddress+7)){ //start, pause, or stop the automation process
		case 0x01:
				startAutomation = 1;
			break;
		case 0x02:
				pauseAutomation = 1;
			break;

		case 0x03:
				stopAutomation = 1;
			break;

		}
	break;

	case 0x07://serial passthrough command
		switch(*(startingAddress+7)){
		case 0x0:
			HAL_UART_Transmit_IT(&huart1, (startingAddress+8), numberOfBytes-2);
			//HAL_GPIO_WritePin(D_OUT1_GPIO_Port, D_OUT1_Pin, GPIO_PIN_RESET);
			break;
		case 0x01:
				enableSerialPassthrough = 1;
				break;
		case 0x02:
				enableSerialPassthrough = 0;
				break;
		}
	break;

	case 0x08: //load parameters
		switch(*(startingAddress + 7)){
		case 0x81:
			parseCoordinate(&global_settings.PickupLocation, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.PickupLocationSet = 1;
		break;
		case 0x82:
			parseCoordinate(&global_settings.PreheatLocation, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.PreheatLocationSet = 1;
		break;
		case 0x83:
			parseCoordinate(&global_settings.FluxFountainEntryLocation, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.FluxFountainEntryLocationSet = 1;
		break;
		case 0x84:
			parseCoordinate(&global_settings.FluxFountainWettingLocation1, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.FluxFountainWettingLocation1Set = 1;
		break;
		case 0x85:
			parseCoordinate(&global_settings.FluxFountainWettingLocation2, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.FluxFountainWettingLocation2Set = 1;
		break;
		case 0x86:
			parseCoordinate(&global_settings.SolderPotEntryLocation, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.SolderPotEntryLocationSet = 1;
		break;
		case 0x87:
			parseCoordinate(&global_settings.SolderPotWettingLocation1, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.SolderPotWettingLocation1Set = 1;
		break;
		case 0x88:
			parseCoordinate(&global_settings.SolderPotWettingLocation2, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.SolderPotWettingLocation2Set = 1;
		break;
		case 0x89:
			parseCoordinate(&global_settings.DropoffLocation, startingAddress+8, numberOfBytes-2);//two bytes from the load parameters, then the specific parameter to be loaded
			global_settings.DropoffLocationSet = 1;
		break;
		case 0x8A:
			parseFloat(&global_settings.ClearZHeight, startingAddress+8);
			global_settings.ClearZHeightSet = 1;
		break;
		case 0x8B:
			parseInt(&global_settings.PreheatSoakTime, startingAddress+8);
			global_settings.PreheatSoakTimeSet = 1;
		break;
		case 0x8C:
			parseInt(&global_settings.TravelSpeed, startingAddress+8);
			global_settings.TravelSpeedSet = 1;
		break;
		case 0x8D:
			parseInt(&global_settings.ActionSpeed, startingAddress+8);
			global_settings.ActionSpeedSet = 1;
		break;
		}
		break;

		case 0x09: //Heartbeats and general requests
			switch(*(startingAddress + 7)){
			case 0x22:
				heartbeatReceived = 1;
				writeToHost(heartbeatByte,2);
			break;

			case 0x23:
				heartbeatReceived = 1;
				heartbeatByte[4] = *(((uint8_t*)(&inputWord) + 1)) ^ 0xf0;
				heartbeatByte[5] = *(((uint8_t*)(&inputWord))) ^ 0xf0;
				heartbeatByte[6] = *(((uint8_t*)(&outputWord) + 1))^ 0xf0;
				heartbeatByte[7] = *(((uint8_t*)(&outputWord)))^ 0xf0;
				heartbeatByte[8] = *(((uint8_t*)(&machineStatusWord) + 1))^ 0xf0;
				heartbeatByte[9] = *(((uint8_t*)(&machineStatusWord)))^ 0xf0;
				writeToHost(heartbeatByte+2,8);
			break;

			}
	break;

	default:

		__NOP();
	break;

	}



	return 0;
}

int scmp(char *strOne, char *strTwo, int len){
	if(len > 255){
		return 0;
	}
	for(int i = 0; i < len; i++){
		if(strOne[i] != strTwo[i]){
			return 0;
		}
	}
	return 1;
}

int parseMsgFromMotion(char *buffer, int len){
	if(len > 150){
		return 0; //proli garbage or parameters just ignore.
	}

	for(int i = 0; i < len; i++){
		if(buffer[i] == '<'){ //start of status
			if(scmp(buffer+1, "Idle", 4)){
				slave_fb_is_idle();
			}else if(scmp(buffer+1, "Run", 3)){
				slave_fb_is_moving();
			}else if(scmp(buffer+1, "Hold", 4)){
				slave_fb_is_idle();
			}else{
				slave_fb_is_idle();
			}
			return 1;
		}
	}

	return 0;
}

void requestMotionStatusUpdate(void){
	uint8_t *buffer = (uint8_t*)"?\n\r";
	int status = HAL_UART_Transmit(&huart1, buffer, 3,400);
	if(status == 2){
		huart1.gState = HAL_UART_STATE_READY;
		status = HAL_UART_Transmit(&huart1, buffer, 3, 400);
	}
}

