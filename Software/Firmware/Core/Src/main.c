/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "startup.h"
#include <string.h>
//#include "state_machine.h"
#include "util.h"
#include "plc_emulator.h"
#include "solderfountain.h"
#include "fluxfountain.h"
#include "input_track.h"
#include "output_track.h"
#include "preheater.h"
#include "state_machine.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */
int _write(int file, char *ptr, int len)
{
  /* Implement your write code here, this is used by puts and printf for example */
  int i=0;
  for(i=0 ; i<len ; i++)
    ITM_SendChar((*ptr++));
  return len;
}

volatile uint16_t inputWord = 0;
volatile uint16_t outputWord = 0;
volatile uint16_t machineStatusWord = 0;


uint8_t connectedToHost = 0;
uint8_t receivingCommands = 0;
volatile uint8_t heartbeatReceived = 0;


//uint8_t confirmKey[] = {0xFF, 0xF0, 0x0F, 0x00};

uint16_t receivedDataFromHost = 0;
uint8_t transmittedDataToMotionController = 0;
uint8_t receivedDataFromMontionController = 0;
uint8_t transmittedDataToHost = 0;
uint8_t messageReceived = 0;

uint16_t rx3Start = 0;
uint16_t rx3End = 0;

uint8_t internalRequest = 0;

volatile uint8_t huart3RXBuffer[1000]; //Buffer for data received from the host over RS232
volatile uint8_t huart3TXBuffer[1000];
volatile uint8_t huart1RXBuffer[1000]; //Buffer for data received from the motion controller over TTL
volatile uint8_t huart1TXBuffer[1000];
volatile uint16_t rx1Start = 0;
volatile uint16_t rx1End = 0;

uint32_t RX1tickTimer = 0;
uint32_t RX1Timeout = 1000; //wait 500ms for a message to complete
uint32_t RX1ReadComplete = 0;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	__NOP();
	if(huart == &huart3){

	}else if(huart == &huart1){
		//transmittedDataToMotionController = 1;
	}
}




void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart == &huart3){

		if(!receivingCommands){//connectedToHost &&
			if(*(huart3RXBuffer + rx3End) == 0x18){//Cancel buffer
					rx3End = -1;
					receivingCommands = 0;}
				/*
				else if((*(huart3RXBuffer + rx3End) == 0x22)){	//is a heartbeat, or synchronous idle
					//HAL_GPIO_WritePin(D_OUT5_GPIO_Port, D_OUT5_Pin, GPIO_PIN_RESET);
					heartbeatReceived = 1;
					HAL_UART_Transmit_IT(&huart3, (uint8_t*)heartbeatByte, 1);
					rx3End--;
				}
				else if((*(huart3RXBuffer + rx3End) == 0x23)){	//is a heartbeat requesting status
					//HAL_GPIO_WritePin(D_OUT6_GPIO_Port, D_OUT6_Pin, GPIO_PIN_RESET);
					heartbeatReceived = 1;
					huart3TXBuffer[0] = *(heartbeatByte+1);
					huart3TXBuffer[1] = *(((uint8_t*)(&inputWord) + 1));
					huart3TXBuffer[2] = *(((uint8_t*)(&inputWord)));
					huart3TXBuffer[3] = *(((uint8_t*)(&outputWord) + 1));
					huart3TXBuffer[4] = *(((uint8_t*)(&outputWord)));
					HAL_UART_Transmit_IT(&huart3, (uint8_t*)huart3TXBuffer, 5);
					rx3End--;
					}*/
				else if(*(huart3RXBuffer + rx3End) == 0x01){//start of header
					receivingCommands = 1;
			}
		}else if((*(huart3RXBuffer + rx3End) == 0x04) && (rx3End > 5)){ // end of transmission reached
			//HAL_GPIO_WritePin(D_OUT4_GPIO_Port, D_OUT4_Pin, GPIO_PIN_RESET);
			parseDataFromHost((uint8_t*)huart3RXBuffer);
			rx3End = 0;
			receivingCommands = 0;
			HAL_UART_Receive_IT(&huart3, (uint8_t*)huart3RXBuffer + rx3End, 1);
			return;
		}

		rx3End++;

		HAL_UART_Receive_IT(&huart3, (uint8_t*)huart3RXBuffer + rx3End, 1);

	}else if(huart == &huart1){
		if(*(huart1RXBuffer + rx1End) == '\n'){
			RX1tickTimer = HAL_GetTick();
			RX1ReadComplete = 1;
		}
		rx1End++;
		HAL_UART_Receive_IT(&huart1, (uint8_t*)(huart1RXBuffer + rx1End), 1);
	}
}

void updateRegisterStatus(){
	outputWord = 0;
	inputWord = 0;
	inputWord |= ((HAL_GPIO_ReadPin(D_IN0_GPIO_Port, D_IN0_Pin) == GPIO_PIN_SET) << 0) | ((HAL_GPIO_ReadPin(D_IN1_GPIO_Port, D_IN1_Pin) == GPIO_PIN_SET) << 1) | ((HAL_GPIO_ReadPin(D_IN2_GPIO_Port, D_IN2_Pin) == GPIO_PIN_SET) << 2) | ((HAL_GPIO_ReadPin(D_IN3_GPIO_Port, D_IN3_Pin) == GPIO_PIN_SET) << 3) | ((HAL_GPIO_ReadPin(D_IN4_GPIO_Port, D_IN4_Pin) == GPIO_PIN_SET) << 4) | ((HAL_GPIO_ReadPin(D_IN5_GPIO_Port, D_IN5_Pin) == GPIO_PIN_SET) << 5) | ((HAL_GPIO_ReadPin(D_IN6_GPIO_Port, D_IN6_Pin) == GPIO_PIN_SET) << 6)| ((HAL_GPIO_ReadPin(D_IN7_GPIO_Port, D_IN7_Pin) == GPIO_PIN_SET) << 7) | ((HAL_GPIO_ReadPin(D_IN8_GPIO_Port, D_IN8_Pin) == GPIO_PIN_SET) << 8);
	outputWord |= ((HAL_GPIO_ReadPin(D_OUT0_GPIO_Port, D_OUT0_Pin) == GPIO_PIN_SET) << 0) | ((HAL_GPIO_ReadPin(D_OUT1_GPIO_Port, D_OUT1_Pin) == GPIO_PIN_SET) << 1) |  ((HAL_GPIO_ReadPin(D_OUT2_GPIO_Port, D_OUT2_Pin) == GPIO_PIN_SET) << 2) |  ((HAL_GPIO_ReadPin(D_OUT3_GPIO_Port, D_OUT3_Pin) == GPIO_PIN_SET) << 3) | ((HAL_GPIO_ReadPin(D_OUT4_GPIO_Port, D_OUT4_Pin) == GPIO_PIN_SET) << 4) | ((HAL_GPIO_ReadPin(D_OUT5_GPIO_Port, D_OUT5_Pin) == GPIO_PIN_SET) << 5) | ((HAL_GPIO_ReadPin(D_OUT6_GPIO_Port, D_OUT6_Pin) == GPIO_PIN_SET) << 6) | ((HAL_GPIO_ReadPin(D_OUT7_GPIO_Port, D_OUT7_Pin) == GPIO_PIN_SET) << 7) |  ((HAL_GPIO_ReadPin(D_OUT8_GPIO_Port, D_OUT8_Pin) == GPIO_PIN_SET) << 8) | ((HAL_GPIO_ReadPin(D_OUT9_GPIO_Port, D_OUT9_Pin) == GPIO_PIN_SET) << 9) |  ((HAL_GPIO_ReadPin(D_OUT10_GPIO_Port, D_OUT10_Pin) == GPIO_PIN_SET) << 10);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if((GPIO_Pin == PIN_PICKUP_PART)){ //part passed pickup sensor
		it_part_detected();
	}else if((GPIO_Pin == PIN_OUTPUTTRACK_CLEARED)){ //part passed output sensor
		ot_part_detected();
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim == &htim4){
			updateRegisterStatus();
	}else if(htim == &htim3){
		nextLoop();//runs at 250hz
		//HAL_GPIO_TogglePin(MASTER_STATUS_LED_GPIO_Port, MASTER_STATUS_LED_Pin);
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */

  ff_stop_cb(); //reset flux fountain
  sf_setup(); //reset solder fountain
  it_setup(); //reset input track solenoids
  ot_setup(); //reset output track solenoids
  preheater_setup(); //reset ic preheater
  ot_assist_part(); //clear output tube if needed
  HAL_GPIO_WritePin(D_OUT7_GPIO_Port,D_OUT7_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(D_OUT8_GPIO_Port,D_OUT8_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(D_OUT9_GPIO_Port,D_OUT9_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(D_OUT10_GPIO_Port,D_OUT10_Pin,GPIO_PIN_RESET);

  //AICT_State.currentState = init; //starts the state machine in the startup state
  //HAL_GPIO_WritePin(MASTER_STATUS_LED_GPIO_Port, MASTER_STATUS_LED_Pin, GPIO_PIN_RESET);
  HAL_UART_Receive_IT(&huart3, (uint8_t*)huart3RXBuffer, 1);
  HAL_UART_Receive_IT(&huart1, (uint8_t*)huart1RXBuffer, 1);

  HAL_TIM_Base_Start_IT(&htim4);
  HAL_TIM_Base_Start_IT(&htim3);
  RX1tickTimer = HAL_GetTick();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int counter = 0;
  while (1)
  {
	  if(AICTState.currentState != standby && !requestPaused){
		  if(counter-- == 0){
		  counter = 1000/20;
		  requestMotionStatusUpdate();
		  internalRequest = 1;
		  }
	  }
	  HAL_Delay(20);
	  uint32_t currentTick = HAL_GetTick();
	  plce_time_update(currentTick);
    
	  if(((currentTick - RX1tickTimer) > RX1Timeout) && RX1ReadComplete){
		  if(enableSerialPassthrough | 1){
			  for(int i = 0; i < rx1End; i++){
	  				huart3TXBuffer[i] = huart1RXBuffer[i];
	  			}
			  if(parseMsgFromMotion((char*)huart3TXBuffer, rx1End) && internalRequest){
				  internalRequest = 0;
			  }else{
			  writeMsgToHost((uint8_t*)huart3TXBuffer, rx1End);
			  }
			  //HAL_GPIO_WritePin(D_OUT0_GPIO_Port, D_OUT0_Pin, GPIO_PIN_RESET);
		  	  	  }else{//must be processed by the micro
		  	  		 __NOP();
		  	  	  }
			RX1ReadComplete = 0;
			rx1End = 0;
			HAL_UART_AbortReceive_IT(&huart1);
			HAL_UART_Receive_IT(&huart1, (uint8_t*)huart1RXBuffer, 1);
	  	  	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 200;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 9600;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 1000;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 9600;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */
  HAL_TIM_Base_MspInit(&htim4);
  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */
  HAL_UART_MspInit(&huart3);
  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, MASTER_STATUS_LED_Pin|ERROR_LED_Pin|DIAGNOSTIC_LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, D_OUT0_Pin|D_OUT1_Pin|D_OUT7_Pin|D_OUT8_Pin
                          |D_OUT9_Pin|D_OUT10_Pin|D_OUT2_Pin|D_OUT3_Pin
                          |D_OUT4_Pin|D_OUT5_Pin|D_OUT6_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : MASTER_STATUS_LED_Pin ERROR_LED_Pin DIAGNOSTIC_LED_Pin */
  GPIO_InitStruct.Pin = MASTER_STATUS_LED_Pin|ERROR_LED_Pin|DIAGNOSTIC_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : D_IN0_Pin D_IN2_Pin */
  GPIO_InitStruct.Pin = D_IN0_Pin|D_IN2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : D_IN1_Pin D_IN3_Pin D_IN4_Pin D_IN5_Pin
                           D_IN6_Pin D_IN7_Pin D_IN8_Pin */
  GPIO_InitStruct.Pin = D_IN1_Pin|D_IN3_Pin|D_IN4_Pin|D_IN5_Pin
                          |D_IN6_Pin|D_IN7_Pin|D_IN8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : D_OUT0_Pin D_OUT1_Pin D_OUT7_Pin D_OUT8_Pin
                           D_OUT9_Pin D_OUT10_Pin D_OUT2_Pin D_OUT3_Pin
                           D_OUT4_Pin D_OUT5_Pin D_OUT6_Pin */
  GPIO_InitStruct.Pin = D_OUT0_Pin|D_OUT1_Pin|D_OUT7_Pin|D_OUT8_Pin
                          |D_OUT9_Pin|D_OUT10_Pin|D_OUT2_Pin|D_OUT3_Pin
                          |D_OUT4_Pin|D_OUT5_Pin|D_OUT6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
