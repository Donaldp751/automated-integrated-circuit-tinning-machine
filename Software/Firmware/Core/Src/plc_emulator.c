#include "plc_emulator.h"
#include <stdlib.h>
#include "stm32f1xx_hal.h"

#define NULL ((void *)0)

struct plce_timer_s
{
	uint32_t target_time;
	void (*function_to_call)(void);

	struct plce_timer_s* next;

};

#define TIME_ERROR_MS 1000

struct plce_timer_s * head = NULL;

//
static int plce_add_to_list(struct plce_timer_s * s);
static int plce_remove_from_list(struct plce_timer_s * s);

// External Functions

int plce_time_update(uint32_t time_ms)
{
	struct plce_timer_s * current = head;
	struct plce_timer_s * next;

	if(current == NULL)
		return 0;


	while(current != NULL)
	{
		next = current->next;

		if(current->target_time < time_ms)
		{
			current->function_to_call();

			plce_remove_from_list(current);

			if((time_ms - current->target_time) > TIME_ERROR_MS)
			{
				// catch?
			}
		}

		current = next;
	}


	return 0;
}

int plce_cleanup(){
	struct plce_timer_s* tmpPtr;
	if(head == NULL)
		{
			return 0;
		}
	else{
		tmpPtr = head;
		while(tmpPtr != NULL){
			if(tmpPtr->next == NULL){
				free(tmpPtr); //end of list
				head = NULL;
				break;
				}else{
					tmpPtr = tmpPtr->next;
					free(head);
				}
			head = tmpPtr;
			}
		}
	return 0;
}

int plce_add_callback(uint32_t time_from_now_ms, void (*function_to_call)(void))
{
	// assuming theres a heap
	struct plce_timer_s * new_timer;


	new_timer = malloc(sizeof(struct plce_timer_s));
	new_timer->function_to_call = function_to_call;
	new_timer->target_time = time_from_now_ms + HAL_GetTick();

	if(plce_add_to_list(new_timer))
		return -1;
	else
		return 0;
}

// Internal Function
static int plce_add_to_list(struct plce_timer_s * s)
{
	if(head == NULL)
	{
		head = s;
		s->next = NULL;
	}
	else
	{
		// add it to the beginning
		// if this timing becomes an issue we can insert it in sorted order or something
		s->next = head;
		head = s;
	}

	return 0;
}

static int plce_remove_from_list(struct plce_timer_s * s)
{
	struct plce_timer_s * previous = NULL;
	struct plce_timer_s * current;

	if(head == NULL)
	{
		//Assert(0);
		return -1;
	}

	for(current = head; current != NULL; current = current->next)
	{
		if(current == s)
		{
			// remove s from the linked list

			if(previous != NULL)
			{
				previous->next = current->next;
			}
			else // s is head
			{
				head = current->next;
			}

			// free s
			free(s);
			return 0;
		}

		previous = current;
	}

	return 0;
}
