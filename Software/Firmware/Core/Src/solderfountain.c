/*
 * solderfountain.c
 *
 *  Created on: Sep 8, 2020
 *      Author: dposterick
 */

#include "main.h"
#include "solderfountain.h"
#include "plc_emulator.h"

int sf_setup(void)
{
	sf_start_off();
	sf_stop();
	return 0;
}

int sf_start(void)
{
	HAL_GPIO_WritePin(PORT_SOLDER_FOUNTAIN_START, PIN_SOLDER_FOUNTAIN_START, GPIO_PIN_SET);
	plce_add_callback(500, sf_start_off);
	return 0;
}
void sf_start_off(void)
{
	HAL_GPIO_WritePin(PORT_SOLDER_FOUNTAIN_START, PIN_SOLDER_FOUNTAIN_START, GPIO_PIN_RESET);
}


int sf_stop(void)
{
	HAL_GPIO_WritePin(PORT_SOLDER_FOUNTAIN_STOP, PIN_SOLDER_FOUNTAIN_STOP, GPIO_PIN_SET);
	plce_add_callback(500, sf_stop_off);
	return 0;
}
void sf_stop_off(void)
{
	HAL_GPIO_WritePin(PORT_SOLDER_FOUNTAIN_STOP, PIN_SOLDER_FOUNTAIN_STOP, GPIO_PIN_RESET);
}
